<!DOCTYPE html>
<html lang="en" data-ng-app="ICeoApp">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{titlePage}}</title>
    <link rel="stylesheet" href="/bower_components/mobile-angular-ui/dist/css/mobile-angular-ui-base.min.css" />
    <link rel="stylesheet" href="/bower_components/mobile-angular-ui/dist/css/mobile-angular-ui-desktop.min.css" />
    <link rel="stylesheet" href="/bower_components/mobile-angular-ui/dist/css/mobile-angular-ui-hover.min.css" />
    <link rel="stylesheet" href="/bower_components/ng-responsive-calendar/dist/css/calendar.min.css" />
    <link rel="stylesheet" href="/bower_components/angular-moment-picker/dist/angular-moment-picker.min.css" />
    <link rel="stylesheet" href="/bower_components/angular-busy/dist/angular-busy.min.css" />
    <link rel="stylesheet" href="/css/style.css" />
</head>
<body data-cg-busy="busyPromise">
<div class="app" data-ng-view></div>
<div data-ui-yield-to="modals"></div>
<div data-ng-if="isAuthenticated" class="sidebar sidebar-left">
    <div class="scrollable">
        <h1 class="scrollable-header app-name">iCEO</h1>
        <div class="scrollable-content">
            <div class="list-group" data-ui-turn-off='uiSidebarLeft'>
                <a class="list-group-item" data-ng-href="/">Home<i class="fa fa-home pull-right"></i></a>
                <a class="list-group-item" data-ng-href="/social-proof"><i class="fa fa-image pull-right"></i>Social Proof</a>
                <a class="list-group-item" data-ng-href="/ceo-profile"><i class="fa fa-globe pull-right"></i>CEO</a>
                <a class="list-group-item child" data-ng-href="/ceo-goals"><i class="fa fa-flag pull-right"></i>CEO Goals</a>
                <a class="list-group-item child" data-ng-href="/calendar"><i class="fa fa-calendar pull-right"></i>CEO Calendar</a>
                <a class="list-group-item child" data-ng-href="/lesson-plans"><i class="fa fa-list-alt pull-right"></i>Lesson Plan</a>
                <a class="list-group-item child" data-ng-href="/notes"><i class="fa fa-sticky-note-o pull-right"></i>Notes</a>
                <a class="list-group-item child" data-ng-href="/ceo-scoreboard"><i class="fa fa-bar-chart pull-right"></i>Scoreboard</a>
                <a class="list-group-item child-x-2" data-ng-href="/apprentice-report"><i class="fa fa-universal-access pull-right"></i>Apprentice</a>
                <a class="list-group-item child-x-2" data-ng-href="/deals-report"><i class="fa fa-trophy pull-right"></i>Deals</a>
                <a class="list-group-item child-x-2" data-ng-href="/financial-report"><i class="fa fa-usd pull-right"></i>Financial</a>
                <a class="list-group-item" data-ng-href="/groups"><i class="fa fa-group pull-right"></i>Groups</a>
                <a data-ng-if="isAdmin" class="list-group-item" data-ng-href="/users"><i class="fa fa-user pull-right"></i>Users</a>
                <a class="list-group-item" data-ng-click="logout()">Logout<i class="fa fa-sign-out pull-right"></i></a>
            </div>
        </div>
    </div>
</div>
<script src="/bower_components/angular/angular.min.js"></script>
<script src="/bower_components/angular-route/angular-route.min.js"></script>
<script src="/bower_components/angular-messages/angular-messages.min.js"></script>
<script src="/bower_components/angular-touch/angular-touch.min.js"></script>
<script src="/bower_components/ng-file-upload/ng-file-upload.min.js"></script>
<script src="/bower_components/chart.js/dist/Chart.min.js"></script>
<script src="/bower_components/angular-chart.js/dist/angular-chart.min.js"></script>
<script src="/bower_components/angular-ui-mask/dist/mask.min.js"></script>
<script src="/bower_components/ng-responsive-calendar/dist/js/calendar-tpls.min.js"></script>
<script src="/bower_components/ng-currency/dist/ng-currency.js"></script>
<script src="/bower_components/moment/min/moment-with-locales.min.js"></script>
<script src="/bower_components/angular-carousel/dist/angular-carousel.min.js"></script>
<script src="/bower_components/angular-moment-picker/dist/angular-moment-picker.min.js"></script>
<script src="/bower_components/angular-busy/dist/angular-busy.min.js"></script>
<script src="/bower_components/mobile-angular-ui/dist/js/mobile-angular-ui.min.js"></script>
<script src="/bower_components/mobile-angular-ui/dist/js/mobile-angular-ui.gestures.min.js"></script>
<script src="/bower_components/angular-jwt/dist/angular-jwt.min.js"></script>
<script src="/js/app.js"></script>
<script src="/js/config.js"></script>
<script src="/js/services/AuthService.js"></script>
<script src="/js/services/APIService.js"></script>
<script src="/js/directives/CustomErrorDirective.js"></script>
<script src="/js/controllers/AuthorizationCtrl.js"></script>
<script src="/js/controllers/HomeCtrl.js"></script>
<script src="/js/controllers/SocialProofCtrl.js"></script>
<script src="/js/controllers/SocialProofEntryCtrl.js"></script>
<script src="/js/controllers/CeoProfileCtrl.js"></script>
<script src="/js/controllers/NotificationsCtrl.js"></script>
<script src="/js/controllers/GroupsCtrl.js"></script>
<script src="/js/controllers/GroupCtrl.js"></script>
<script src="/js/controllers/MemberCtrl.js"></script>
<script src="/js/controllers/AddMemberCtrl.js"></script>
<script src="/js/controllers/EditMemberCtrl.js"></script>
<script src="/js/controllers/CeoScoreboardCtrl.js"></script>
<script src="/js/controllers/ApprenticeReportCtrl.js"></script>
<script src="/js/controllers/DealsReportCtrl.js"></script>
<script src="/js/controllers/FinancialReportCtrl.js"></script>
<script src="/js/controllers/MemberGoalsCtrl.js"></script>
<script src="/js/controllers/MemberScorecardCtrl.js"></script>
<script src="/js/controllers/ContractsCtrl.js"></script>
<script src="/js/controllers/AddContractCtrl.js"></script>
<script src="/js/controllers/EditContractCtrl.js"></script>
<script src="/js/controllers/ContractCtrl.js"></script>
<script src="/js/controllers/CeoGoalsCtrl.js"></script>
<script src="/js/controllers/CalendarCtrl.js"></script>
<script src="/js/controllers/LessonPlansCtrl.js"></script>
<script src="/js/controllers/LessonPlanCtrl.js"></script>
<script src="/js/controllers/NotesCtrl.js"></script>
<script src="/js/controllers/UsersCtrl.js"></script>
<script src="/js/route.js"></script>
</body>
</html>