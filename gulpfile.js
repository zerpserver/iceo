var gulp        = require('gulp'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    cssmin      = require('gulp-cssmin'),
    flatten     = require('gulp-flatten'),
    ngAnnotate  = require('gulp-ng-annotate'),
    filter      = require('gulp-filter');

var files = [
    "./bower_components/mobile-angular-ui/dist/css/mobile-angular-ui-base.min.css",
    "./bower_components/mobile-angular-ui/dist/css/mobile-angular-ui-desktop.min.css",
    "./bower_components/mobile-angular-ui/dist/css/mobile-angular-ui-hover.min.css",
    "./bower_components/ng-responsive-calendar/dist/css/calendar.min.css",
    "./bower_components/angular-moment-picker/dist/angular-moment-picker.min.css",
    "./bower_components/angular-busy/dist/angular-busy.min.css",
    "./bower_components/angular/angular.min.js",
    "./bower_components/angular-route/angular-route.min.js",
    "./bower_components/angular-messages/angular-messages.min.js",
    "./bower_components/angular-touch/angular-touch.min.js",
    "./bower_components/ng-file-upload/ng-file-upload.min.js",
    "./bower_components/chart.js/dist/Chart.min.js",
    "./bower_components/angular-chart.js/dist/angular-chart.min.js",
    "./bower_components/angular-ui-mask/dist/mask.min.js",
    "./bower_components/ng-responsive-calendar/dist/js/calendar-tpls.min.js",
    "./bower_components/ng-currency/dist/ng-currency.js",
    "./bower_components/moment/min/moment-with-locales.min.js",
    "./bower_components/angular-carousel/dist/angular-carousel.min.js",
    "./bower_components/angular-moment-picker/dist/angular-moment-picker.min.js",
    "./bower_components/angular-busy/dist/angular-busy.min.js",
    "./bower_components/mobile-angular-ui/dist/js/mobile-angular-ui.min.js",
    "./bower_components/mobile-angular-ui/dist/js/mobile-angular-ui.gestures.min.js",
    "./bower_components/mobile-angular-ui/dist/js/mobile-angular-ui.gestures.min.js.map",
    "./bower_components/angular-jwt/dist/angular-jwt.min.js",
    "./bower_components/mobile-angular-ui/dist/fonts/fontawesome-webfont.eot",
    "./bower_components/mobile-angular-ui/dist/fonts/fontawesome-webfont.svg",
    "./bower_components/mobile-angular-ui/dist/fonts/fontawesome-webfont.ttf",
    "./bower_components/mobile-angular-ui/dist/fonts/fontawesome-webfont.woff",
    "./bower_components/mobile-angular-ui/dist/fonts/fontawesome-webfont.woff2"
];

gulp.task('components', function(){

    var jsFilter    = filter('**/*.js', {restore: true});
    var mapFilter   = filter('**/*.js.map', {restore: true});
    var cssFilter   = filter('**/*.css', {restore: true});
    var fontFilter  = filter(['**/*.eot', '**/*.woff', '**/*.woff2', '**/*.svg', '**/*.ttf']);

    return gulp.src(files)
        .pipe(jsFilter)
        .pipe(concat('components.js'))
        .pipe(gulp.dest('./assets/js/'))
        .pipe(jsFilter.restore)
        .pipe(mapFilter)
        .pipe(gulp.dest('./assets/js/'))
        .pipe(mapFilter.restore)
        .pipe(cssFilter)
        .pipe(concat('components.css'))
        .pipe(gulp.dest('./assets/css/'))
        .pipe(cssFilter.restore)
        .pipe(fontFilter)
        .pipe(flatten())
        .pipe(gulp.dest('./assets/fonts/'));

});

gulp.task('scripts', function(){

    return gulp.src(['./js/app.js', './js/**/*.js'])
        .pipe(concat('app.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest('./assets/js/'));

});

gulp.task('css', function(){

    return gulp.src('./css/**/*.css')
        .pipe(concat('style.css'))
        .pipe(cssmin())
        .pipe(gulp.dest('./assets/css/'));

});

gulp.task('default', ['components', 'scripts', 'css']);