(function(){
    'use strict';

    angular.module('ICeoApp', [
        'ngRoute',
        'ngMessages',
        'ngTouch',
        'ngFileUpload',
        'chart.js',
        'ui.mask',
        'ui.rCalendar',
        'ng-currency',
        'moment-picker',
        'angular-carousel',
        'cgBusy',
        'mobile-angular-ui',
        'angular-jwt'
    ]);
})();