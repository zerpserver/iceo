(function(){
    'use strict';

    angular.module('ICeoApp').config(config);

    config.$inject = ['$routeProvider', '$locationProvider'];

    /* @ngInject */
    function config($routeProvider, $locationProvider) {
        $routeProvider.
        when('/', {
            templateUrl: '/templates/home.html',
            controller: 'HomeCtrl'
        }).
        when('/auth', {
            templateUrl: '/templates/authorization.html',
            controller: 'AuthorizationCtrl'
        }).
        when('/social-proof', {
            templateUrl: '/templates/social-proof.html',
            controller: 'SocialProofCtrl'
        }).
        when('/social-proof/:entryId', {
            templateUrl: '/templates/social-proof-entry.html',
            controller: 'SocialProofEntryCtrl'
        }).
        when('/ceo-profile', {
            templateUrl: '/templates/ceo-profile.html',
            controller: 'CeoProfileCtrl'
        }).
        when('/notifications', {
            templateUrl: '/templates/notifications.html',
            controller: 'NotificationsCtrl'
        }).
        when('/groups', {
            templateUrl: '/templates/groups.html',
            controller: 'GroupsCtrl'
        }).
        when('/group/:groupId', {
            templateUrl: '/templates/group.html',
            controller: 'GroupCtrl'
        }).
        when('/member/add', {
            templateUrl: '/templates/edit-member.html',
            controller: 'AddMemberCtrl'
        }).
        when('/member/:memberId', {
            templateUrl: '/templates/member.html',
            controller: 'MemberCtrl'
        }).
        when('/member/:memberId/goals', {
            templateUrl: '/templates/member-goals.html',
            controller: 'MemberGoalsCtrl'
        }).
        when('/member/:memberId/scorecard', {
            templateUrl: '/templates/member-scorecard.html',
            controller: 'MemberScorecardCtrl'
        }).
        when('/member/edit/:memberId', {
            templateUrl: '/templates/edit-member.html',
            controller: 'EditMemberCtrl'
        }).
        when('/member/:memberId/contract/list', {
            templateUrl: '/templates/contracts.html',
            controller: 'ContractsCtrl'
        }).
        when('/member/:memberId/contract/add', {
            templateUrl: '/templates/edit-contract.html',
            controller: 'AddContractCtrl'
        }).
        when('/member/:memberId/contract/:contractId', {
            templateUrl: '/templates/contract.html',
            controller: 'ContractCtrl'
        }).
        when('/member/:memberId/contract/:contractId/edit', {
            templateUrl: '/templates/edit-contract.html',
            controller: 'EditContractCtrl'
        }).
        when('/ceo-scoreboard', {
            templateUrl: '/templates/ceo-scoreboard.html',
            controller: 'CeoScoreboardCtrl'
        }).
        when('/apprentice-report', {
            templateUrl: '/templates/apprentice-report.html',
            controller: 'ApprenticeReportCtrl'
        }).
        when('/deals-report', {
            templateUrl: '/templates/deals-report.html',
            controller: 'DealsReportCtrl'
        }).
        when('/financial-report', {
            templateUrl: '/templates/financial-report.html',
            controller: 'FinancialReportCtrl'
        }).
        when('/ceo-goals', {
            templateUrl: '/templates/ceo-goals.html',
            controller: 'CeoGoalsCtrl'
        }).
        when('/calendar', {
            templateUrl: '/templates/calendar.html',
            controller: 'CalendarCtrl'
        }).
        when('/lesson-plans', {
            templateUrl: '/templates/lesson-plans.html',
            controller: 'LessonPlansCtrl'
        }).
        when('/lesson-plans/:groupId', {
            templateUrl: '/templates/lesson-plan.html',
            controller: 'LessonPlanCtrl'
        }).
        when('/notes', {
            templateUrl: '/templates/notes.html',
            controller: 'NotesCtrl'
        }).
        when('/users', {
            templateUrl: '/templates/users.html',
            controller: 'UsersCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }
})();