(function(){
    'use strict';

    angular.module('ICeoApp').directive('customError', CustomErrorDirective);

    function CustomErrorDirective() {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ctrl) {
                element.on('click', function () {
                    scope.$apply(function () {
                        ctrl.$setValidity('custom', true);
                    });
                });
            }
        };
    }
})();