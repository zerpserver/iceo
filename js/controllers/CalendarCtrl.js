(function(){
    'use strict';

    angular.module('ICeoApp').controller('CalendarCtrl', CalendarCtrl);

    CalendarCtrl.$inject = ['$scope', '$rootScope', '$templateCache', 'SharedState', 'APIService'];

    /* @ngInject */
    function CalendarCtrl($scope, $rootScope, $templateCache, SharedState, APIService) {
        $rootScope.titlePage = 'Calendar';

        $templateCache.put("template/rcalendar/month.html",
            "<div>\n" +
            "    <table class=\"table table-bordered table-fixed monthview-datetable monthview-datetable\">\n" +
            "        <thead>\n" +
            "        <tr>\n" +
            "            <th ng-show=\"showWeeks\" class=\"calendar-week-column text-center\">#</th>\n" +
            "            <th ng-repeat=\"label in labels track by $index\" class=\"text-center\">\n" +
            "                <small>{{label}}</small>\n" +
            "            </th>\n" +
            "        </tr>\n" +
            "        </thead>\n" +
            "        <tbody>\n" +
            "        <tr ng-repeat=\"row in rows track by $index\">\n" +
            "            <td ng-show=\"showWeeks\" class=\"calendar-week-column text-center\">\n" +
            "                <small><em>{{ weekNumbers[$index] }}</em></small>\n" +
            "            </td>\n" +
            "            <td ng-repeat=\"dt in row track by dt.date\" class=\"monthview-dateCell\" ng-click=\"select(dt.date)\"\n" +
            "                ng-class=\"{'text-center':true, 'monthview-current': dt.current&&!dt.selected&&!dt.hasEvent,'monthview-secondary-with-event': dt.secondary&&dt.hasEvent, 'monthview-primary-with-event':!dt.secondary&&dt.hasEvent&&!dt.selected, 'monthview-selected': dt.selected}\">\n" +
            "                <div ng-class=\"{'text-muted':dt.secondary}\">\n" +
            "                    {{dt.label}}\n" +
            "                </div>\n" +
            "            </td>\n" +
            "        </tr>\n" +
            "        </tbody>\n" +
            "    </table>\n" +
            "    <div ng-if=\"showEventDetail\" class=\"event-detail-container\">\n" +
            "        <div class=\"scrollable\">\n" +
            "            <table class=\"table table-bordered table-striped table-fixed\">\n" +
            "                <tr ng-style=\"event.style\" ng-repeat=\"event in selectedDate.events\" ng-if=\"selectedDate.events\">\n" +
            "                    <td ng-if=\"!event.allDay\" class=\"monthview-eventdetail-timecolumn\">{{event.startTime|date: 'HH:mm'}}\n" +
            "                        -\n" +
            "                        {{event.endTime|date: 'HH:mm'}}\n" +
            "                    </td>\n" +
            "                    <td ng-if=\"event.allDay\" class=\"monthview-eventdetail-timecolumn\">{{ event.type }}</td>\n" +
            "                    <td class=\"event-detail\" ng-click=\"eventSelected({event:event})\">{{event.title}}</td>\n" +
            "                </tr>\n" +
            "                <tr ng-if=\"!selectedDate.events\"><td class=\"no-event-label\">No Events</td></tr>\n" +
            "            </table>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>");

        $scope.events = [];
        
        APIService.getCeoCalendar().success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                angular.forEach(response.contracts, function (contract) {

                    var date = moment(contract.contract_expiration);

                    $scope.events.push({
                        title: contract.address + ' - ' + contract.apprentice.first_name + ' ' + contract.apprentice.last_name,
                        type: 'Contract',
                        style: { backgroundColor: '#f5c748' },
                        startTime: date,
                        endTime: date.add(1, 'd'),
                        allDay: true
                    });

                });

                angular.forEach(response.events, function (event) {

                    var date = moment(event.due_date);

                    $scope.events.push({
                        title: event.title,
                        type: 'Custom',
                        style: { backgroundColor: '#4ea26c' },
                        startTime: date,
                        endTime: date.add(1, 'd'),
                        allDay: true
                    });

                });

                $scope.$broadcast('eventSourceChanged',$scope.events);

            }

        });

        $scope.addEvent = function () {

            var self = this;

            if (this.FormEvent.$dirty && this.FormEvent.$valid){

                APIService.addCeoCalendar(this.event).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        var date = moment(response.event.due_date);

                        $scope.events.push({
                            title: response.event.title,
                            type: 'Custom',
                            style: { backgroundColor: '#4ea26c' },
                            startTime: date,
                            endTime: date.add(1, 'd'),
                            allDay: true
                        });

                        $scope.$broadcast('eventSourceChanged',$scope.events);
                        SharedState.turnOff('modalCalendarEvent');

                    }
                    else{

                        self.FormEvent.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormEvent.hasOwnProperty(key)){
                                self.FormEvent[key].$setValidity('custom', false);
                                self.FormEvent.errors[key] = value[0];
                            }

                        });

                    }

                });

            }

        };
    }
})();