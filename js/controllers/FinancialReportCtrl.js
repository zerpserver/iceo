(function(){
    'use strict';

    angular.module('ICeoApp').controller('FinancialReportCtrl', FinancialReportCtrl);

    FinancialReportCtrl.$inject = ['$scope', '$rootScope', 'APIService'];

    /* @ngInject */
    function FinancialReportCtrl($scope, $rootScope, APIService) {
        $rootScope.titlePage = 'Financial Report';

        APIService.getCeoReportFinancial().success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.ceo = response.ceo;

            }

        });
    }
})();