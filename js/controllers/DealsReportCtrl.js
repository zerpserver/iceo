(function(){
    'use strict';

    angular.module('ICeoApp').controller('DealsReportCtrl', DealsReportCtrl);

    DealsReportCtrl.$inject = ['$scope', '$rootScope', 'APIService'];

    /* @ngInject */
    function DealsReportCtrl($scope, $rootScope, APIService) {
        $rootScope.titlePage = 'Deals Report';

        APIService.getCeoReportDeals().success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.ceo = response.ceo;

            }

        });
    }
})();