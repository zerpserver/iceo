(function(){
    'use strict';

    angular.module('ICeoApp').controller('CeoProfileCtrl', CeoProfileCtrl);

    CeoProfileCtrl.$inject = ['$scope', '$rootScope'];

    /* @ngInject */
    function CeoProfileCtrl($scope, $rootScope) {
        $rootScope.titlePage = 'Ceo Profile';
    }
})();