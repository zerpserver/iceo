(function(){
    'use strict';

    angular.module('ICeoApp').controller('LessonPlanCtrl', LessonPlanCtrl);

    LessonPlanCtrl.$inject = ['$scope', '$rootScope', '$routeParams', 'SharedState', 'APIService'];

    /* @ngInject */
    function LessonPlanCtrl($scope, $rootScope, $routeParams, SharedState, APIService) {

        APIService.getCeoLessonGroup({ id: $routeParams.groupId }).success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.group = response.group;
                $rootScope.titlePage = $scope.title = 'Lesson Plan - ' + $scope.group.title;

            }

        });

        $scope.addLesson = function () {
            $scope.lesson = {};

            SharedState.turnOn('modalLesson');
        };

        $scope.editLesson = function (lesson) {

            $scope.lesson = {
                id: lesson.id,
                title: lesson.title
            };

            SharedState.turnOn('modalLessonEdit');
        };

        $scope.removeLesson = function (lesson) {

            APIService.removeCeoLesson({ id: lesson.id }).success(function (response) {

                if (angular.isDefined(response.success) && response.success){

                    var length = $scope.group.lessons.length;

                    for (var i = 0; i < length; i++){

                        if ($scope.group.lessons[i].id == lesson.id){

                            $scope.group.lessons.splice(i, 1);
                            break;

                        }

                    }

                }

            });

        };

        $scope.createLesson = function (lesson) {

            var self = this;

            if (this.FormLessonAdd.$dirty && this.FormLessonAdd.$valid){

                APIService.addCeoLesson({ title: lesson.title, file: lesson.file, group_id: $scope.group.id }).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        $scope.group.lessons.push(response.lesson);

                        SharedState.turnOff('modalLesson');
                    }
                    else{

                        self.FormLessonAdd.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormLessonAdd.hasOwnProperty(key)){
                                self.FormLessonAdd[key].$setValidity('custom', false);
                                self.FormLessonAdd.errors[key] = value[0];
                            }

                        });

                    }

                });

            }

        };

        $scope.updateLesson = function (lesson) {

            var self = this;

            if (this.FormLessonEdit.$valid){

                APIService.updateCeoLesson({ id: lesson.id, title: lesson.title }).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        var length = $scope.group.lessons.length;

                        for (var i = 0; i < length; i++){

                            if ($scope.group.lessons[i].id == lesson.id){

                                $scope.group.lessons[i] = response.lesson;
                                break;

                            }

                        }

                        SharedState.turnOff('modalLessonEdit');
                    }
                    else{

                        self.FormLessonEdit.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormLessonEdit.hasOwnProperty(key)){
                                self.FormLessonEdit[key].$setValidity('custom', false);
                                self.FormLessonEdit.errors[key] = value[0];
                            }

                        });

                    }

                });

            }

        };
    }
})();