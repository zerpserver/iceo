(function(){
    'use strict';

    angular.module('ICeoApp').controller('AddContractCtrl', AddContractCtrl);

    AddContractCtrl.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'APIService'];

    /* @ngInject */
    function AddContractCtrl($scope, $rootScope, $routeParams, $location, APIService) {

        $rootScope.titlePage = $scope.title = 'Contract Details';

        APIService.getApprentice({ id: $routeParams.memberId }).success(function (response) {

            if (angular.isDefined(response.success) && response.success){
                $scope.apprentice = response.apprentice;
                $scope.contract = {
                    apprentice_id: response.apprentice.id
                };

                if ($scope.apprentice.main_exit_strategy){

                    $scope.contract.exit_strategy_id = $scope.apprentice.main_exit_strategy;

                }

            }

        });

        APIService.getExitStrategies().success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.exit_strategies = response.exit_strategies;

            }

        });

        $scope.save = function(){

            this.FormContract.$setSubmitted();

            if (this.FormContract.$dirty && this.FormContract.$valid){

                APIService.addContract($scope.contract).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){
                        $location.path('/member/' + $scope.apprentice.id + '/contract/' + response.contract.id);
                    }
                    else{
                        $scope.FormContract.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if ($scope.FormContract.hasOwnProperty(key)){
                                $scope.FormContract[key].$setValidity('custom', false);
                                $scope.FormContract.errors[key] = value[0];
                            }

                        });
                    }

                });

            }
        };
    }
})();