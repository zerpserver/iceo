(function(){
    'use strict';

    angular.module('ICeoApp').controller('UsersCtrl', UsersCtrl);

    UsersCtrl.$inject = ['$scope', '$rootScope', 'APIService', '$location', 'SharedState'];

    /* @ngInject */
    function UsersCtrl($scope, $rootScope, APIService, $location, SharedState) {
        $rootScope.titlePage = 'Users';

        if (!$rootScope.isAdmin){
            $location.path('/');
            return;
        }

        APIService.getCeoUsers().success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.users = response.users;

            }

        });

        $scope.showEdit = function (user) {

            $scope.edit_user = {
                id: user.id,
                name: user.name,
                email: user.email,
                active: user.active
            };

            SharedState.turnOn('modalEditUser');

        };

        $scope.removeUser = function (user) {

            APIService.removeCeoUser({ id: user.id }).success(function (response) {

                if (angular.isDefined(response.success) && response.success){

                    var length = $scope.users.length;

                    for(var i = 0; i < length; i++){

                        if ($scope.users[i].id == user.id){

                            $scope.users.splice(i, 1);
                            break;

                        }

                    }

                    SharedState.turnOff('modalEditUser');

                }

            });

        };

        $scope.editUser = function (edit_user) {

            var self = this;

            if (this.FormEditUser.$valid){

                APIService.updateCeoUser(edit_user).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        var length = $scope.users.length;

                        for(var i = 0; i < length; i++){

                            if ($scope.users[i].id == response.user.id){

                                $scope.users[i] = response.user;
                                break;

                            }

                        }

                        SharedState.turnOff('modalEditUser');

                    }
                    else{

                        self.FormEditUser.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormEditUser.hasOwnProperty(key)){
                                self.FormEditUser[key].$setValidity('custom', false);
                                self.FormEditUser.errors[key] = value[0];
                            }

                        });

                    }

                });

            }

        };

        $scope.addUser = function () {

            var self = this;

            if (this.FormAddUser.$dirty && this.FormAddUser.$valid){

                APIService.addCeoUser(this.user).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        $scope.users.push(response.user);
                        $scope.username = response.user.name;

                        SharedState.turnOff('modalAddUser');
                        SharedState.turnOn('modalSuccess');

                    }
                    else{
                        self.FormAddUser.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormAddUser.hasOwnProperty(key)){
                                self.FormAddUser[key].$setValidity('custom', false);
                                self.FormAddUser.errors[key] = value[0];
                            }

                        });
                    }

                });

            }

        };

    }
})();