(function(){
    'use strict';

    angular.module('ICeoApp').controller('CeoGoalsCtrl', CeoGoalsCtrl);

    CeoGoalsCtrl.$inject = ['$scope', '$rootScope', 'SharedState', 'APIService'];

    /* @ngInject */
    function CeoGoalsCtrl($scope, $rootScope, SharedState, APIService) {
        $rootScope.titlePage = 'CEO Goals';

        APIService.getCeoGoals().success(function (response) {

            if (angular.isDefined(response.success) && response.success){
                $scope.ceo = response.ceo;
            }

        });

        $scope.updateTarget = function (target) {

            APIService.updateCeoTarget(target);

        };

        $scope.updateGoal = function (goal) {
            APIService.updateCeoGoal({ id: goal.id, status: goal.status });
        };

        $scope.enter = function (event) {

            if (event.which === 13){

                event.preventDefault();
                $scope.addGoal.call(this);

            }

        };

        $scope.addGoal = function () {

            var self = this;

            this.FormAddGoal.$setSubmitted();

            if (this.FormAddGoal.$dirty && this.FormAddGoal.$valid){

                APIService.addCeoGoal({ title: this.goal.title }).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        $scope.ceo.goals.push(response.goal);

                        SharedState.turnOff('modalAddGoal');

                    }
                    else{
                        self.FormAddGoal.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormAddGoal.hasOwnProperty(key)){
                                self.FormAddGoal[key].$setValidity('custom', false);
                                self.FormAddGoal.errors[key] = value[0];
                            }

                        });
                    }

                });

            }

        };
    }
})();