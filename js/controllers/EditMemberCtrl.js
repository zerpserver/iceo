(function(){
    'use strict';

    angular.module('ICeoApp').controller('EditMemberCtrl', EditMemberCtrl);

    EditMemberCtrl.$inject = ['$scope', '$rootScope', '$routeParams', '$window', '$location', 'APIService'];

    /* @ngInject */
    function EditMemberCtrl($scope, $rootScope, $routeParams, $window, $location, APIService) {
        $rootScope.titlePage = $scope.title = 'Edit Apprentice';

        APIService.getApprentice({ id: $routeParams.memberId }).success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.apprentice = {
                    id: response.apprentice.id,
                    first_name: response.apprentice.first_name,
                    last_name: response.apprentice.last_name,
                    phone: response.apprentice.phone,
                    email: response.apprentice.email,
                    current_salary: response.apprentice.current_salary,
                    image: response.apprentice.image,
                    group_id: response.apprentice.group_id
                };

            }

        });

        APIService.getGroups().success(function (response) {

            if (angular.isDefined(response.success) && response.success){
                $scope.groups = response.groups;
            }

        });

        $scope.save = function(){

            $scope.FormMember.$setSubmitted();

            if ($scope.FormMember.$valid){

                APIService.updateApprentice($scope.apprentice).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){
                        $location.path('/group/' + response.apprentice.group_id);
                    }
                    else{
                        $scope.FormMember.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if ($scope.FormMember.hasOwnProperty(key)) {
                                $scope.FormMember[key].$setValidity('custom', false);
                                $scope.FormMember.errors[key] = value[0];
                            }

                        });
                    }

                });
            }
        };

        $scope.back = function(){
            $window.history.back();
        };
    }
})();