(function(){
    'use strict';

    angular.module('ICeoApp').controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['$scope', '$rootScope'];

    /* @ngInject */
    function HomeCtrl($scope, $rootScope) {
        $rootScope.titlePage = 'Home';
    }
})();