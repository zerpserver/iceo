(function(){
    'use strict';

    angular.module('ICeoApp').controller('CeoScoreboardCtrl', CeoScoreboardCtrl);

    CeoScoreboardCtrl.$inject = ['$scope', '$rootScope', 'APIService'];

    /* @ngInject */
    function CeoScoreboardCtrl($scope, $rootScope, APIService) {
        $rootScope.titlePage = 'Ceo Scoreboard';

        APIService.getCeoScoreboard().success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.ceo = response.ceo;

                if (! $scope.ceo.target){

                    $scope.ceo.target = {
                        apprentice_target: 0,
                        deals_target: 0,
                        profit_target: 0
                    };

                }

                var remaining_apprentices = $scope.ceo.target.apprentice_target - $scope.ceo.apprentices;
                var remaining_deals = $scope.ceo.target.deals_target - $scope.ceo.deals;
                var remaining_profit = $scope.ceo.target.profit_target - $scope.ceo.profit_total;


                remaining_apprentices = remaining_apprentices < 0 ? 0 : remaining_apprentices;
                remaining_deals = remaining_deals < 0 ? 0 : remaining_deals;
                remaining_profit = remaining_profit < 0 ? 0 : remaining_profit;

                $scope.charts = {
                    apprentices: {
                        data: [$scope.ceo.apprentices, remaining_apprentices],
                        labels: ["Apprentices", "To goal"]
                    },
                    deals: {
                        data: [$scope.ceo.deals, remaining_deals],
                        labels: ["Closed deals", "To goal"]
                    },
                    financial: {
                        data: [$scope.ceo.profit_total, remaining_profit],
                        labels: ["Current profit", "To goal"]
                    }
                };

            }

        });

    }
})();