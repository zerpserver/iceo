(function(){
    'use strict';

    angular.module('ICeoApp').controller('SocialProofEntryCtrl', SocialProofEntryCtrl);

    SocialProofEntryCtrl.$inject = ['$scope', '$rootScope', '$routeParams', 'APIService', 'SharedState'];

    /* @ngInject */
    function SocialProofEntryCtrl($scope, $rootScope, $routeParams, APIService, SharedState) {
        $rootScope.titlePage = $scope.title = 'Social Proof';

        APIService.getSocialTask({ id: $routeParams.entryId }).success(function(response){
            if (angular.isDefined(response.success) && response.success){
                $scope.task = response.task;
                $scope.title = response.task.title;
            }
        });

        $scope.showFile = function (entry) {

            $scope.entry = {
                id: entry.id,
                ceo_id: entry.ceo_id,
                proof_image_url: entry.proof_image_url,
                remark: entry.remark,
                type: entry.type
            };

            SharedState.turnOn('modalFile');
        };

        $scope.isImage = function (type) {

            return /^image/.test(type);

        };

        $scope.isVideo = function (type) {

            return /^video/.test(type);

        };

        $scope.removeEntry = function (entry) {
            var data = [entry.id];

            if (data.length){

                APIService.removeSocialEntries({entry_ids: data}).success(function (response) {
                    if (angular.isDefined(response.success) && response.success){

                        var length = $scope.task.entries.length;

                        for (var i = 0; i < length; i++){

                            if ($scope.task.entries[i].id == entry.id){
                                $scope.task.entries.splice(i, 1);
                                break;
                            }

                        }

                        SharedState.turnOff('modalFile');

                    }
                });

            }

        };

        $scope.updateEntry = function (entry) {

            var self = this;

            if (this.FormEntry.$valid){

                APIService.updateSocialTask(entry).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        var length = $scope.task.entries.length;

                        for (var i = 0; i < length; i++){

                            if ($scope.task.entries[i].id == response.entry.id){

                                $scope.task.entries[i] = response.entry;
                                break;

                            }

                        }

                        SharedState.turnOff('modalFile');

                    }
                    else{

                        self.FormEntry.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormEntry.hasOwnProperty(key)){
                                self.FormEntry[key].$setValidity('custom', false);
                                self.FormEntry.errors[key] = value[0];
                            }

                        });

                    }

                });

            }

        };

        $scope.upload = function ($files, $invalidFiles, task) {

            $scope.errors = [];

            if ($invalidFiles.length === 0){

                if ($files.length){

                    APIService.createSocialTask({
                        task_id: task.id,
                        files: $files
                    }).success(function (response) {

                        if (angular.isDefined(response.success) && response.success){

                            task.entries = response.entries;

                        }
                        else{

                            angular.forEach(response.errors, function (error) {
                                $scope.errors = $scope.errors.concat(error);
                            });

                            SharedState.turnOn('modalErrors');

                        }

                    }).then(function () {
                        delete task.files;
                    });
                }
            }
            else{

                $scope.invalidFiles = $invalidFiles;
                SharedState.turnOn('modalErrors');

            }
        };

    }
})();