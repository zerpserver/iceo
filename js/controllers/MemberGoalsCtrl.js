(function(){
    'use strict';

    angular.module('ICeoApp').controller('MemberGoalsCtrl', MemberGoalsCtrl);

    MemberGoalsCtrl.$inject = ['$scope', '$rootScope', '$routeParams', 'APIService', 'SharedState'];

    /* @ngInject */
    function MemberGoalsCtrl($scope, $rootScope, $routeParams, APIService, SharedState) {
        $rootScope.titlePage = 'Apprentice Goals';

        APIService.getApprenticeGoals({ id: $routeParams.memberId }).success(function (response) {

            if (angular.isDefined(response.success) && response.success){
                $scope.apprentice = response.apprentice;
            }

        });

        $scope.updateGoal = function (goal) {
            APIService.updateApprenticeGoal({ id: $scope.apprentice.id, goal_id: goal.id, status: goal.status });
        };

        $scope.enter = function (event) {

            if (event.which === 13){

                event.preventDefault();
                $scope.addGoal.call(this);

            }

        };

        $scope.addGoal = function () {

            var self = this;

            if (this.FormAddGoal.$dirty && this.FormAddGoal.$valid){

                APIService.addApprenticeGoal({ id: $scope.apprentice.id, title: this.goal.title }).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        $scope.apprentice.goals.push(response.goal);

                        SharedState.turnOff('modalAddGoal');

                    }
                    else{
                        self.FormAddGoal.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormAddGoal.hasOwnProperty(key)){
                                self.FormAddGoal[key].$setValidity('custom', false);
                                self.FormAddGoal.errors[key] = value[0];
                            }

                        });
                    }

                });

            }

        };

    }
})();