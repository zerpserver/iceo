(function(){
    'use strict';

    angular.module('ICeoApp').controller('NotesCtrl', NotesCtrl);

    NotesCtrl.$inject = ['$scope', '$rootScope', 'APIService', 'SharedState'];

    /* @ngInject */
    function NotesCtrl($scope, $rootScope, APIService, SharedState) {
        $rootScope.titlePage = 'Notes';

        function setDate(note) {
            var date = moment(note.updated_at);

            note.date = date.format('MM/DD/YYYY hh:mm A');
        }

        APIService.getCeoNotes().success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.ceo = response.ceo;

                if (angular.isArray($scope.ceo.notes) && $scope.ceo.notes.length){

                    $scope.ceo.notes.map(setDate);

                }

            }

        });

        $scope.addNote = function () {

            $scope.entry = {
                title: 'New Note',
                note: ''
            };

            SharedState.turnOn('modalNote');
        };

        $scope.editNote = function (entry) {

            $scope.entry = {
                id: entry.id,
                title: 'Edit Note',
                note: entry.note
            };

            SharedState.turnOn('modalNote');
        };

        $scope.saveNote = function (entry) {

            if (!angular.isDefined(entry.note) || !entry.note) return;

            if (angular.isDefined(entry.id) && entry.id){

                APIService.updateCeoNote(entry).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        var length = $scope.ceo.notes.length;

                        setDate(response.note);

                        for (var i = 0; i < length; i++){

                            if ($scope.ceo.notes[i].id == response.note.id){
                                $scope.ceo.notes[i] = response.note;
                                break;
                            }

                        }

                        SharedState.turnOff('modalNote');

                    }

                });

            }
            else{

                APIService.addCeoNote(entry).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        setDate(response.note);

                        $scope.ceo.notes.unshift(response.note);

                        SharedState.turnOff('modalNote');

                    }

                });

            }

        };

        $scope.removeNote = function (entry) {

            APIService.removeCeoNote({ note_ids: entry.id }).success(function (response) {

                if (angular.isDefined(response.success) && response.success){

                    var length = $scope.ceo.notes.length;

                    for (var i = 0; i < length; i++){

                        if ($scope.ceo.notes[i].id == entry.id){
                            $scope.ceo.notes.splice(i, 1);
                            break;
                        }

                    }

                    SharedState.turnOff('modalNote');
                }

            });

        };
    }
})();