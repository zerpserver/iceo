(function(){
    'use strict';

    angular.module('ICeoApp').controller('GroupCtrl', GroupCtrl);

    GroupCtrl.$inject = ['$scope', '$rootScope', '$routeParams', 'SharedState', 'APIService', '$location', '$filter'];

    /* @ngInject */
    function GroupCtrl($scope, $rootScope, $routeParams, SharedState, APIService, $location, $filter) {

        APIService.getGroups().success(function (response) {
            if (angular.isDefined(response.success) && response.success){
                $scope.groups = response.groups;
            }
        });

        APIService.getGroup({ id: $routeParams.groupId }).success(function (response) {
            if (angular.isDefined(response.success) && response.success){
                $rootScope.titlePage = response.group.title + ' Group';
                $scope.group = response.group;
                $scope.change_group = $scope.group.id;
            }
            else{
                $location.path('/groups');
            }
        });

        $scope.cancelApprentices = function(){
            $filter('filter')($scope.group.apprentices, {checked: true}).map(function(apprentice){ delete apprentice.checked; });
        };

        $scope.removeApprentices = function(){
            var data = $filter('filter')($scope.group.apprentices, {checked: true}).map(function(apprentice){ return apprentice.id });

            APIService.removeApprentices({ apprentice_ids: data }).success(function (response) {

                if (angular.isDefined(response.success) && response.success){
                    var i = 0;

                    while(i < $scope.group.apprentices.length) {
                        if ($scope.group.apprentices[i].checked) {
                            $scope.group.apprentices.splice(i, 1);
                        } else {
                            i++;
                        }
                    }

                    SharedState.turnOff('membersEdit');
                }

            });
        };

        $scope.showConfirm = function(){

            if($filter('filter')($scope.group.apprentices, {checked: true}).length){
                SharedState.turnOn('modalConfirm');
            }
            else{
                SharedState.turnOff('membersEdit');
            }

        };

        $scope.changeGroup = function () {
            var data = $filter('filter')($scope.group.apprentices, {checked: true});

            if (data.length){

                SharedState.turnOn('modalGroups');

            }
        };

        $scope.moveGroup = function () {

            if ($scope.group.id == this.change_group){

                SharedState.turnOff('modalGroups');
                return;

            }

            var self = this;
            var ids = $filter('filter')($scope.group.apprentices, {checked: true}).map(function(apprentice){ return apprentice.id });

            APIService.moveGroup({
                apprentice_ids: ids,
                group_id: this.change_group
            }).success(function (response) {

                if (angular.isDefined(response.success) && response.success){

                    var i = 0;

                    while(i < $scope.group.apprentices.length) {
                        if ($scope.group.apprentices[i].checked) {
                            $scope.group.apprentices.splice(i, 1);
                        } else {
                            i++;
                        }
                    }

                    SharedState.turnOff('modalGroups');
                    SharedState.turnOff('membersEdit');
                    SharedState.turnOn('modalGroupsSuccess');

                }
                else{

                    self.FormMoveGroup.errors = {};

                    angular.forEach(response.errors, function (value, key) {

                        if (self.FormMoveGroup.hasOwnProperty(key)){
                            self.FormMoveGroup[key].$setValidity('custom', false);
                            self.FormMoveGroup.errors[key] = value[0];
                        }

                    });

                }

            });

        };
    }
})();