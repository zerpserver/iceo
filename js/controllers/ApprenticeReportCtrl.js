(function(){
    'use strict';

    angular.module('ICeoApp').controller('ApprenticeReportCtrl', ApprenticeReportCtrl);

    ApprenticeReportCtrl.$inject = ['$scope', '$rootScope', 'APIService'];

    /* @ngInject */
    function ApprenticeReportCtrl($scope, $rootScope, APIService) {
        $rootScope.titlePage = 'Apprentice Report';

        APIService.getCeoReportApprentices().success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.ceo = response.ceo;

            }

        });

        $scope.orderByField = 'first_name';
        $scope.reverseSort = false;
    }
})();