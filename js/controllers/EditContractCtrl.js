(function(){
    'use strict';

    angular.module('ICeoApp').controller('EditContractCtrl', EditContractCtrl);

    EditContractCtrl.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'APIService'];

    /* @ngInject */
    function EditContractCtrl($scope, $rootScope, $routeParams, $location, APIService) {

        $rootScope.titlePage = $scope.title = 'Edit Contract';

        $scope.isEdit = true;

        $scope.status_list = [
            {
                id: 0,
                title: 'Open'
            },
            {
                id: 1,
                title: 'Closed'
            },
            {
                id: 2,
                title: 'Lost'
            }
        ];

        APIService.getContract({ id: $routeParams.contractId }).success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.contract = {
                    id: response.contract.id,
                    address: response.contract.address,
                    apprentice_id: response.contract.apprentice_id,
                    apprentice_profit: response.contract.apprentice_profit,
                    city: response.contract.city,
                    contract_expiration: response.contract.contract_expiration,
                    date_of_contract: response.contract.date_of_contract,
                    exit_strategy_id: response.contract.exit_strategy_id,
                    profit_amount: response.contract.profit_amount,
                    seller_name: response.contract.seller_name,
                    state: response.contract.state,
                    zip: response.contract.zip,
                    status: response.contract.status
                };

            }
            else{

                $location.path('/member/' + $routeParams.memberId + '/scorecard');

            }

        });

        APIService.getExitStrategies().success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.exit_strategies = response.exit_strategies;

            }

        });

        $scope.save = function(){

            this.FormContract.$setSubmitted();

            if (this.FormContract.$valid){

                APIService.updateContract($scope.contract).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){
                        $location.path('/member/' + response.contract.apprentice_id + '/contract/' + response.contract.id);
                    }
                    else{
                        $scope.FormContract.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if ($scope.FormContract.hasOwnProperty(key)){
                                $scope.FormContract[key].$setValidity('custom', false);
                                $scope.FormContract.errors[key] = value[0];
                            }

                        });
                    }

                });

            }
        };
    }
})();