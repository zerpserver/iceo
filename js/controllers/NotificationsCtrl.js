(function(){
    'use strict';

    angular.module('ICeoApp').controller('NotificationsCtrl', NotificationsCtrl);

    NotificationsCtrl.$inject = ['$scope', '$rootScope'];

    /* @ngInject */
    function NotificationsCtrl($scope, $rootScope) {
        $rootScope.titlePage = 'Notifications';
    }
})();