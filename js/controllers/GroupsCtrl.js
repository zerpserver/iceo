(function(){
    'use strict';

    angular.module('ICeoApp').controller('GroupsCtrl', GroupsCtrl);

    GroupsCtrl.$inject = ['$scope', '$rootScope', '$filter', 'SharedState', 'APIService'];

    /* @ngInject */
    function GroupsCtrl($scope, $rootScope, $filter, SharedState, APIService) {
        $rootScope.titlePage = 'Groups';

        $scope.groups = [];

        APIService.getGroups().success(function (response) {
            if (angular.isDefined(response.success) && response.success){
                $scope.groups = response.groups;
            }
        });

        $scope.addGroup = function(){

            var self = this;

            if (this.FormAddGroup.$dirty && this.FormAddGroup.$valid){
                
                APIService.addGroup(this.group).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        $scope.groups.push(response.group);
                        SharedState.turnOff('modalAddGroup');

                    }
                    else{

                        self.FormAddGroup.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormAddGroup.hasOwnProperty(key)){
                                self.FormAddGroup[key].$setValidity('custom', false);
                                self.FormAddGroup.errors[key] = value[0];
                            }

                        });

                    }

                });

            }

        };

        $scope.modalEditGroup = function (group){
            if (group){
                $scope.edit_group = {};
                $scope.edit_group.id = group.id;
                $scope.edit_group.title = group.title;

                SharedState.turnOn('modalEditGroup');
            }
        };

        $scope.editGroup = function(){

            var self = this;

            if (this.FormEditGroup.$valid){

                if ($scope.edit_group){

                    APIService.updateGroup($scope.edit_group).success(function (response) {

                        if (angular.isDefined(response.success) && response.success){

                            var length = $scope.groups.length;

                            for (var i = 0; i < length; i++){
                                if ($scope.groups[i].id === response.group.id){
                                    $scope.groups[i] = response.group;
                                    break;
                                }
                            }

                            SharedState.turnOff('modalEditGroup');
                        }
                        else{

                            self.FormEditGroup.errors = {};

                            angular.forEach(response.errors, function (value, key) {

                                if (self.FormEditGroup.hasOwnProperty(key)){
                                    self.FormEditGroup[key].$setValidity('custom', false);
                                    self.FormEditGroup.errors[key] = value[0];
                                }

                            });

                        }

                    });

                }

            }
        };

        $scope.cancelGroups = function(){
            $filter('filter')($scope.groups, {checked: true}).map(function(group){ delete group.checked; });
        };

        $scope.removeGroups = function(){
            var data = $filter('filter')($scope.groups, {checked: true}).map(function(group){ return group.id });

            if (data.length){
                APIService.removeGroups({group_ids: data}).success(function (response) {
                    if (angular.isDefined(response.success) && response.success){
                        var i = 0;

                        while(i < $scope.groups.length) {
                            if ($scope.groups[i].checked) {
                                $scope.groups.splice(i, 1);
                            } else {
                                i++;
                            }
                        }

                    }
                });
            }

            SharedState.turnOff('modalConfirm');
        };
    }
})();