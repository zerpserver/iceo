(function(){
    'use strict';

    angular.module('ICeoApp').controller('AuthorizationCtrl', AuthorizationCtrl);

    AuthorizationCtrl.$inject = ['$scope', '$rootScope', 'AuthService', 'SharedState'];

    /* @ngInject */
    function AuthorizationCtrl($scope, $rootScope, AuthService, SharedState) {
        $rootScope.titlePage = 'Authorization';

        $scope.loginSubmit = function(){

            var self = this;

            if (this.FormLogin.$valid){

                delete this.FormLogin.message;

                AuthService.login(this.user).then(function (response) {

                    if (angular.isDefined(response.success) && angular.isDefined(response.message)){

                        self.FormLogin.message = {
                            'class': response.success ? 'alert-success' : 'alert-danger',
                            'text': response.message
                        };

                    }

                });

            }

        };

        $scope.clearLoginForm = function () {

            delete $scope.FormLogin.message;
            $scope.FormLogin.$setPristine();

        };

        $scope.registerSubmit = function(){

            var self = this;

            if (this.FormRegister.$valid){

                AuthService.register(this.register).success(function (response) {

                    if (angular.isDefined(response.success)){
                        if (response.success){

                            $scope.FormLogin.message = {
                                'class': 'alert-success',
                                'text': response.message
                            };

                            SharedState.turnOff('modalRegister');
                        }
                        else{
                            self.FormRegister.errors = {};

                            angular.forEach(response.errors, function (value, key) {

                                if (self.FormRegister.hasOwnProperty(key)) {
                                    self.FormRegister[key].$setValidity('custom', false);
                                    self.FormRegister.errors[key] = value[0];
                                }

                            });
                        }
                    }

                });

            }
        };

        $scope.forgotSubmit = function(){

            var self = this;

            if (this.FormForgot.$dirty && this.FormForgot.$valid){

                AuthService.forgot(this.forgot).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        SharedState.turnOff('modalForgot');
                        SharedState.turnOn('modalForgotSuccess');

                    }
                    else{

                        self.FormForgot.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormForgot.hasOwnProperty(key)) {
                                self.FormForgot[key].$setValidity('custom', false);
                                self.FormForgot.errors[key] = value[0];
                            }

                        });

                    }

                });

            }

        };
    }
})();