(function(){
    'use strict';

    angular.module('ICeoApp').controller('LessonPlansCtrl', LessonPlansCtrl);

    LessonPlansCtrl.$inject = ['$scope', '$rootScope', 'SharedState', 'APIService'];

    /* @ngInject */
    function LessonPlansCtrl($scope, $rootScope, SharedState, APIService) {
        $rootScope.titlePage = 'Lesson Groups';

        $scope.groups = [];

        APIService.getCeoLessonGroups().success(function (response) {
            if (angular.isDefined(response.success) && response.success){
                $scope.groups = response.groups;
            }
        });

        $scope.addNewGroup = function(){

            var self = this;

            if (this.FormAddGroup.$dirty && this.FormAddGroup.$valid){

                APIService.addCeoLessonGroup({ title: this.group.title }).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){
                        $scope.groups.push(response.group);
                        SharedState.turnOff('modalAddGroup');
                    }
                    else{

                        self.FormAddGroup.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormAddGroup.hasOwnProperty(key)){
                                self.FormAddGroup[key].$setValidity('custom', false);
                                self.FormAddGroup.errors[key] = value[0];
                            }

                        });

                    }

                });

            }
        };

        $scope.modalEditGroup = function (group){
            if (group){
                $scope.edit_group = {};
                $scope.edit_group.id = group.id;
                $scope.edit_group.title = group.title;

                SharedState.turnOn('modalEditGroup');
            }
        };

        $scope.editGroup = function(){

            var self = this;

            if (this.FormEditGroup.$valid){

                if ($scope.edit_group){

                    APIService.updateCeoLessonGroup($scope.edit_group).success(function (response) {

                        if (angular.isDefined(response.success) && response.success){

                            var length = $scope.groups.length;

                            for (var i = 0; i < length; i++){
                                if ($scope.groups[i].id === response.group.id){
                                    $scope.groups[i] = response.group;
                                    break;
                                }
                            }

                            SharedState.turnOff('modalEditGroup');
                        }
                        else{

                            self.FormEditGroup.errors = {};

                            angular.forEach(response.errors, function (value, key) {

                                if (self.FormEditGroup.hasOwnProperty(key)){
                                    self.FormEditGroup[key].$setValidity('custom', false);
                                    self.FormEditGroup.errors[key] = value[0];
                                }

                            });

                        }

                    });

                }

            }
        };

        $scope.removeGroup = function(group){

            APIService.removeCeoLessonGroup({ id: group.id }).success(function (response) {

                if (angular.isDefined(response.success) && response.success){

                    $scope.groups.splice($scope.groups.indexOf(group), 1);

                }

            });

        };
    }
})();