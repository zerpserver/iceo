(function(){
    'use strict';

    angular.module('ICeoApp').controller('AddMemberCtrl', AddMemberCtrl);

    AddMemberCtrl.$inject = ['$scope', '$rootScope', '$window', '$location', 'APIService'];

    /* @ngInject */
    function AddMemberCtrl($scope, $rootScope, $window, $location, APIService) {
        $rootScope.titlePage = $scope.title = 'Add Apprentice';

        APIService.getGroups().success(function (response) {

            if (angular.isDefined(response.success) && response.success){
                $scope.groups = response.groups;
            }

        });

        $scope.save = function(){

            $scope.FormMember.$setSubmitted();

            if ($scope.FormMember.$dirty && $scope.FormMember.$valid){

                APIService.addApprentice($scope.apprentice).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){
                        $location.path('/group/' + response.apprentice.group_id);
                    }
                    else{
                        $scope.FormMember.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if ($scope.FormMember.hasOwnProperty(key)) {
                                $scope.FormMember[key].$setValidity('custom', false);
                                $scope.FormMember.errors[key] = value[0];
                            }

                        });
                    }

                });

            }
        };

        $scope.back = function(){
            $window.history.back();
        };
    }
})();