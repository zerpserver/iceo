(function(){
    'use strict';

    angular.module('ICeoApp').controller('SocialProofCtrl', SocialProofCtrl);

    SocialProofCtrl.$inject = ['$scope', '$rootScope', '$filter', 'APIService', 'SharedState'];

    /* @ngInject */
    function SocialProofCtrl($scope, $rootScope, $filter, APIService, SharedState) {
        $rootScope.titlePage = 'Social Proof';

        $scope.isSend = false;

        function showFile(entry) {

            $scope.entry = {
                id: entry.id,
                ceo_id: entry.ceo_id,
                task_id: entry.task_id,
                proof_image_url: entry.proof_image_url,
                remark: entry.remark,
                type: entry.type
            };

            SharedState.turnOn('modalFile');

        }

        APIService.getSocialTasks().success(function(response){
            if (angular.isDefined(response.success) && response.success){
                $scope.tasks = response.tasks;
                $scope.social_media_manager_email = response.social_media_manager_email;

                angular.forEach($scope.tasks, function (task) {

                    if (task.entries.length){
                        $scope.isSend = true;
                    }

                });
            }
        });

        $scope.isImage = function (type) {

            return /^image/.test(type);

        };

        $scope.isVideo = function (type) {

            return /^video/.test(type);

        };

        $scope.removeEntry = function (entry) {
            var data = [entry.id];

            if (data.length){

                APIService.removeSocialEntries({entry_ids: data}).success(function (response) {
                    if (angular.isDefined(response.success) && response.success){

                        var tasks_length = $scope.tasks.length;

                        for (var i = 0; i < tasks_length; i++){

                            if ($scope.tasks[i].id == entry.task_id){

                                var entries_length = $scope.tasks[i].entries.length;

                                for (var y = 0; y < entries_length; y++){

                                    if ($scope.tasks[i].entries[y].id == entry.id){
                                        $scope.tasks[i].entries.splice(y, 1);
                                        break;
                                    }

                                }

                                break;
                            }

                        }

                        SharedState.turnOff('modalFile');

                    }
                });

            }

        };

        $scope.updateEntry = function (entry) {

            var self = this;

            if (this.FormEntry.$valid){

                APIService.updateSocialTask(entry).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        SharedState.turnOff('modalFile');

                    }
                    else{

                        self.FormEntry.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormEntry.hasOwnProperty(key)){
                                self.FormEntry[key].$setValidity('custom', false);
                                self.FormEntry.errors[key] = value[0];
                            }

                        });

                    }

                });

            }

        };

        $scope.upload = function ($files, $invalidFiles, task) {

            $scope.errors = [];

            if ($invalidFiles.length === 0){

                if ($files.length){

                    APIService.createSocialTask({
                            task_id: task.id,
                            files: $files
                    }).success(function (response) {

                        if (angular.isDefined(response.success) && response.success){

                            task.entries = response.entries;

                            var entry = $filter('filter')(task.entries, {id: Math.max.apply(Math, task.entries.map(function (entry) {
                                return entry.id;
                            }))})[0];

                            showFile(entry);

                            $scope.isSend = true;

                        }
                        else{

                            angular.forEach(response.errors, function (error) {
                                $scope.errors = $scope.errors.concat(error);
                            });

                            SharedState.turnOn('modalErrors');

                        }

                    }).then(function () {
                        delete task.files;
                    });
                }
            }
            else{

                $scope.invalidFiles = $invalidFiles;
                SharedState.turnOn('modalErrors');

            }
        };

        $scope.send = function(){

            var self = this;

            if (this.FormSend.$valid){

                APIService.sendSocialTask({ email: this.social_media_manager_email }).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        angular.forEach($scope.tasks, function (task) {
                            task.entries = [];
                        });

                        $scope.isSend = false;

                        SharedState.turnOff('modalSend');
                        SharedState.turnOn('modalSuccess');

                    }
                    else{

                        self.FormSend.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormSend.hasOwnProperty(key)){
                                self.FormSend[key].$setValidity('custom', false);
                                self.FormSend.errors[key] = value[0];
                            }

                        });

                    }

                });

            }

        };
    }
})();