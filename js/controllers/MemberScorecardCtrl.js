(function(){
    'use strict';

    angular.module('ICeoApp').controller('MemberScorecardCtrl', MemberScorecardCtrl);

    MemberScorecardCtrl.$inject = ['$scope', '$rootScope', '$routeParams', 'APIService'];

    /* @ngInject */
    function MemberScorecardCtrl($scope, $rootScope, $routeParams, APIService) {
        $rootScope.titlePage = 'Apprentice Scorecard';

        APIService.getApprenticeScorecard({ id: $routeParams.memberId }).success(function (response) {

            if (angular.isDefined(response.success) && response.success){
                $scope.apprentice = response.apprentice;
            }

        });

        $scope.changeProspects = function () {

            if (this.FormProspect.$dirty && this.FormProspect.$valid){

                var data = {
                    id: $scope.apprentice.id,
                    no_of_prospects: $scope.apprentice.no_of_prospects
                };

                APIService.updateApprentice(data);
            }

        };

        $scope.saveNotes = function () {

            var data = {
                id: $scope.apprentice.id,
                notes: $scope.apprentice.notes
            };

            APIService.updateApprentice(data);
        }

    }
})();