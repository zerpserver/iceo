(function(){
    'use strict';

    angular.module('ICeoApp').controller('MemberCtrl', MemberCtrl);

    MemberCtrl.$inject = ['$scope', '$rootScope', '$routeParams', '$filter', 'APIService', 'SharedState'];

    /* @ngInject */
    function MemberCtrl($scope, $rootScope, $routeParams, $filter, APIService, SharedState) {

        $scope.calendar = {};

        APIService.getApprenticeProfile({ id: $routeParams.memberId }).success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.apprentice = response.apprentice;
                $rootScope.titlePage = $scope.title = $scope.apprentice.first_name + ' ' + $scope.apprentice.last_name;

            }

        });

        APIService.getGroups().success(function (response) {

            if (angular.isDefined(response.success) && response.success){
                $scope.groups = response.groups;
            }

        });

        APIService.getExitStrategies().success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.exit_strategies = response.exit_strategies;

            }

        });

        APIService.getLeadStrategies().success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $scope.lead_strategies = response.lead_strategies;

            }

        });

        $scope.changeExitStrategy = function () {

            APIService.updateApprentice({
                id: $scope.apprentice.id,
                main_exit_strategy: $scope.apprentice.main_exit_strategy
            });

        };

        $scope.changeLeadStrategy = function () {

            APIService.updateApprentice({
                id: $scope.apprentice.id,
                lead_strategy_1: $scope.apprentice.lead_strategy_1,
                lead_strategy_2: $scope.apprentice.lead_strategy_2,
                lead_strategy_3: $scope.apprentice.lead_strategy_3
            });

        };

        $scope.saveApprentice = function () {

            var self = this;

            if (this.FormApprentice.$dirty && this.FormApprentice.$valid){

                APIService.updateApprentice({
                    id: $scope.apprentice.id,
                    sweet_spots_of_focus: $scope.apprentice.sweet_spots_of_focus
                }).success(function (response) {

                    if (angular.isDefined(response.success) && !response.success){

                        self.FormApprentice.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormApprentice.hasOwnProperty(key)){
                                self.FormApprentice[key].$setValidity('custom', false);
                                self.FormApprentice.errors[key] = value[0];
                            }

                        });

                    }

                });

            }

        };

        $scope.addLeadStrategy = function () {

            var self = this;

            if (this.FormLeadStrategy.$dirty && this.FormLeadStrategy.$valid){

                APIService.addLeadStrategy(this.lead_strategy).success(function (response) {

                    if (angular.isDefined(response.success) && response.success){

                        $scope.lead_strategies.push(response.lead_strategy);
                        SharedState.turnOff('modalLeadStrategy');

                    }
                    else{

                        self.FormLeadStrategy.errors = {};

                        angular.forEach(response.errors, function (value, key) {

                            if (self.FormLeadStrategy.hasOwnProperty(key)){
                                self.FormLeadStrategy[key].$setValidity('custom', false);
                                self.FormLeadStrategy.errors[key] = value[0];
                            }

                        });

                    }

                });

            }

        };

        $scope.changeGroup = function(){

            APIService.updateApprentice({
                id: $scope.apprentice.id,
                group_id: $scope.apprentice.group_id
            });

        };

        $scope.removeUploads = function () {
            $scope.calendar.errors = [];

            var data = $filter('filter')($scope.apprentice.calendar, {checked: true}).map(function(entry){ return entry.id });

            if (data.length){

                APIService.removeCalendar({calendar_ids: data}).success(function (response) {
                    if (angular.isDefined(response.success) && response.success){
                        var i = 0;

                        while(i < $scope.apprentice.calendar.length) {
                            if ($scope.apprentice.calendar[i].checked) {
                                $scope.apprentice.calendar.splice(i, 1);
                            } else {
                                i++;
                            }
                        }
                    }
                });

            }

        };

        $scope.cancelCheckedUploads = function () {
            $scope.calendar.errors = [];

            $filter('filter')($scope.apprentice.calendar, {checked: true}).map(function(entry){ delete entry.checked; });
        };

        $scope.calendarUpload = function ($files, $invalidFiles) {

            delete $scope.invalidFiles;
            $scope.calendar.errors = [];

            if ($invalidFiles.length === 0){

                if ($files.length){

                    APIService.addCalendar({
                        apprentice_id: $scope.apprentice.id,
                        images: $files
                    }).success(function (response) {

                        if (angular.isDefined(response.success)){
                            if (response.success){

                                $scope.apprentice.calendar = $scope.apprentice.calendar.concat(response.calendar);

                            }
                            else{

                                angular.forEach(response.errors, function (error) {
                                    $scope.calendar.errors = $scope.calendar.errors.concat(error);
                                });

                                SharedState.turnOn('modalErrors');

                            }
                        }

                    });

                }

            }
            else{

                $scope.invalidFiles = $invalidFiles;
                SharedState.turnOn('modalErrors');

            }

        };
    }
})();