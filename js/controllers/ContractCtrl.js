(function(){
    'use strict';

    angular.module('ICeoApp').controller('ContractCtrl', ContractCtrl);

    ContractCtrl.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'APIService'];

    /* @ngInject */
    function ContractCtrl($scope, $rootScope, $routeParams, $location, APIService) {

        function setContractLeft(contract) {

            var now     = moment();
            var start   = moment(contract.date_of_contract);
            var end     = moment(contract.contract_expiration);

            if (now > start) start = now;

            contract.left = end.diff(start, 'days');
            contract.left = contract.left < 0 ? 0 : contract.left;

        }

        APIService.getContract({ id: $routeParams.contractId }).success(function (response) {

            if (angular.isDefined(response.success) && response.success){

                $rootScope.titlePage = $scope.title = response.contract.address;

                $scope.contract = response.contract;

                setContractLeft($scope.contract);

            }
            else{
                $location.path('/member/' + $routeParams.memberId + '/scorecard');
            }

        });

        $scope.Closed = function(){

            APIService.setContractClosed({ id: $scope.contract.id }).success(function (response) {

                if (angular.isDefined(response.success) && response.success){

                    $scope.contract = response.contract;

                    setContractLeft($scope.contract);

                }

            });

        };

        $scope.Lost = function(){

            APIService.setContractLost({ id: $scope.contract.id }).success(function (response) {

                if (angular.isDefined(response.success) && response.success){

                    $scope.contract = response.contract;

                    setContractLeft($scope.contract);

                }

            });

        };
    }
})();