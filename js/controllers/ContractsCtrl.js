(function(){
    'use strict';

    angular.module('ICeoApp').controller('ContractsCtrl', ContractsCtrl);

    ContractsCtrl.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'APIService'];

    /* @ngInject */
    function ContractsCtrl($scope, $rootScope, $routeParams, $location, APIService) {
        $rootScope.titlePage = 'Under Contract';

        APIService.getApprenticeContracts({ id: $routeParams.memberId }).success(function (response) {

            if (angular.isDefined(response.success) && response.success){
                $scope.apprentice = response.apprentice;
            }
            else{
                $location.path('/groups');
            }

        });
    }
})();