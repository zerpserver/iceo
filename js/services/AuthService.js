(function(){
    'use strict';

    angular.module('ICeoApp').factory('AuthService', AuthService);

    AuthService.$inject = ['$rootScope', '$http', '$q', 'authManager', 'jwtHelper', 'API', '$location'];

    /* @ngInject */
    function AuthService($rootScope, $http, $q, authManager, jwtHelper, API, $location) {

        function login(credentials) {

            return $q(function(resolve) {

                $rootScope.busyPromise = $http.post(API.url + '/auth/signin', credentials, {
                    skipAuthorization: true
                }).then(function (response) {

                    if (angular.isDefined(response.data.token)){

                        var result = jwtHelper.decodeToken(response.data.token);

                        if (result.hasOwnProperty('is_admin')){
                            $rootScope.isAdmin = result.is_admin;
                        }

                        localStorage.setItem('token', response.data.token);
                        authManager.authenticate();
                        $location.path('/');
                    }

                    resolve(response.data);

                });

            });

        }

        function register(user) {
            return $rootScope.busyPromise = $http.post(API.url + '/auth/signup', user, {
                skipAuthorization: true
            });
        }

        function forgot(email) {
            return $rootScope.busyPromise = $http.post(API.url + '/auth/forgot', email, {
                skipAuthorization: true
            });
        }

        function logout() {
            localStorage.removeItem('token');
            authManager.unauthenticate();
            $location.path('/auth');
        }

        function getToken() {

            var token = localStorage.getItem('token');

            if (token) {

                if (jwtHelper.isTokenExpired(token)) {

                    return $http.get(API.url + '/auth/refresh', {
                        headers: {
                            Authorization: 'Bearer ' + token
                        }
                    }).then(function (response) {

                        var token = response.headers('authorization').replace('Bearer ', '');

                        var result = jwtHelper.decodeToken(token);

                        if (result.hasOwnProperty('is_admin')){
                            $rootScope.isAdmin = result.is_admin;
                        }

                        localStorage.setItem('token', token);
                        return token;

                    }, function () {
                        logout();
                    });

                } else {
                    return token;
                }
            }
        }

        return {
            'login': login,
            'register': register,
            'forgot': forgot,
            'logout': logout,
            'getToken': getToken
        };
    }
})();