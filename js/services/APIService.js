(function(){
    'use strict';

    angular.module('ICeoApp').service('APIService', APIService);

    APIService.$inject = ['$rootScope', '$http', 'API', 'Upload'];

    /* @ngInject */
    function APIService($rootScope, $http, API, Upload) {

        this.getSocialTasks = function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/social-proofs');
        };

        this.getSocialTask = function (data) {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/social-proofs/' + data.id);
        };

        this.createSocialTask = function (data) {
            return $rootScope.busyPromise = Upload.upload({
                url: API.url + API.base + '/social-proofs/create',
                data: data
            });
        };

        this.updateSocialTask = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/social-proofs/update', data);
        };

        this.removeSocialEntries = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/social-proofs/delete', data);
        };

        this.sendSocialTask = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/social-proofs/send', data);
        };

        this.getGroups = function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/groups/list');
        };

        this.getGroup = function (data) {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/groups/' + data.id);
        };

        this.addGroup = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/groups/add', data);
        };
        
        this.updateGroup = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/groups/' + data.id + '/edit', data);
        };

        this.moveGroup = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/groups/move', data);
        };

        this.removeGroups = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/groups/delete', data);
        };

        this.getApprentice = function (data) {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/apprentices/' + data.id);
        };

        this.getApprenticeContracts = function (data) {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/apprentices/' + data.id + '/contracts');
        };

        this.getApprenticeGoals = function (data) {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/apprentices/' + data.id + '/goals');
        };

        this.addApprenticeGoal = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/apprentices/' + data.id + '/goals/add', data);
        };

        this.updateApprenticeGoal = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/apprentices/' + data.id + '/goals/edit', data);
        };

        this.getApprenticeProfile = function (data) {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/apprentices/' + data.id + '/profile');
        };

        this.getApprenticeScorecard = function (data) {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/apprentices/' + data.id + '/scorecard');
        };

        this.addApprentice = function (data) {
            return $rootScope.busyPromise = Upload.upload({
                url: API.url + API.base + '/apprentices/add',
                data: data
            });
        };

        this.updateApprentice = function (data) {
            return $rootScope.busyPromise = Upload.upload({
                url: API.url + API.base + '/apprentices/' + data.id + '/edit',
                data: data
            });
        };

        this.removeApprentices = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/apprentices/delete', data);
        };

        this.getContract = function (data) {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/contracts/' + data.id);
        };

        this.addContract = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/contracts/add', data);
        };

        this.updateContract = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/contracts/edit', data);
        };

        this.setContractLost = function (data) {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/contracts/' + data.id + '/lost');
        };

        this.setContractClosed = function (data) {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/contracts/' + data.id + '/closed');
        };

        this.addCalendar = function (data) {
            return $rootScope.busyPromise = Upload.upload({
                url: API.url + API.base + '/calendar/add',
                data: data
            });
        };

        this.removeCalendar = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/calendar/delete', data);
        };

        this.getCeoGoals =  function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/ceo/goals');
        };

        this.addCeoGoal = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/goals/add', data);
        };

        this.getCeoNotes =  function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/ceo/notes');
        };

        this.getCeoScoreboard =  function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/ceo/scoreboard');
        };

        this.updateCeoTarget =  function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/update/target', data);
        };

        this.updateCeoGoal =  function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/update/goal', data);
        };

        this.addCeoNote =  function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/notes/add', data);
        };

        this.updateCeoNote =  function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/notes/edit', data);
        };

        this.removeCeoNote =  function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/notes/delete', data);
        };

        this.getCeoLessonGroups =  function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/ceo/lesson-groups');
        };

        this.getCeoLessonGroup = function (data) {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/ceo/lesson-groups/' + data.id);
        };

        this.addCeoLessonGroup = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/lesson-groups/add', data);
        };

        this.updateCeoLessonGroup = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/lesson-groups/edit', data);
        };

        this.removeCeoLessonGroup = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/lesson-groups/delete', data);
        };

        this.addCeoLesson =  function (data) {
            return $rootScope.busyPromise = Upload.upload({
                url: API.url + API.base + '/ceo/lessons/add',
                data: data
            });
        };

        this.updateCeoLesson =  function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/lessons/edit', data);
        };

        this.removeCeoLesson =  function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/lessons/delete', data);
        };

        this.getExitStrategies = function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/strategies/exit');
        };

        this.getLeadStrategies = function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/strategies/lead');
        };

        this.addLeadStrategy = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/strategies/lead/add', data);
        };

        this.getCeoReportApprentices = function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/ceo/report/apprentices');
        };

        this.getCeoReportFinancial = function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/ceo/report/financial');
        };

        this.getCeoReportDeals = function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/ceo/report/deals');
        };

        this.getCeoCalendar = function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/ceo/calendar');
        };

        this.addCeoCalendar = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/calendar/add', data);
        };

        this.getCeoUsers = function () {
            return $rootScope.busyPromise = $http.get(API.url + API.base + '/ceo/users');
        };

        this.addCeoUser = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/users/add', data);
        };

        this.updateCeoUser = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/users/edit', data);
        };

        this.removeCeoUser = function (data) {
            return $rootScope.busyPromise = $http.post(API.url + API.base + '/ceo/users/delete', data);
        };

    }
})();