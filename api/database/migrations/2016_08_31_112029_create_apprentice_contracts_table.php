<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprenticeContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apprentice_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apprentice_id', false, true);
            $table->string('seller_name');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->integer('exit_strategy_id', false, true);
            $table->string('profit_amount');
            $table->string('apprentice_profit');
            $table->string('date_of_contract');
            $table->string('contract_expiration');
            $table->enum('status', [0, 1, 2]);
            $table->dateTime('completed_at');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->foreign('apprentice_id')->references('id')->on('apprentices')->onDelete('cascade');
            $table->foreign('exit_strategy_id')->references('id')->on('exit_strategies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apprentice_contracts');
    }
}
