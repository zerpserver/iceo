<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprenticeCalendarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apprentice_calendar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apprentice_id', false, true);
            $table->string('image');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->foreign('apprentice_id')->references('id')->on('apprentices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apprentice_calendar');
    }
}
