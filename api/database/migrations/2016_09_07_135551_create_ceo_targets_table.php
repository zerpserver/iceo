<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCeoTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ceo_targets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id', false, true);
            $table->integer('apprentice_target');
            $table->integer('deals_target');
            $table->integer('profit_target');
            $table->foreign('account_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ceo_targets');
    }
}
