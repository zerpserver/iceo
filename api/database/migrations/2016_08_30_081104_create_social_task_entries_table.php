<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialTaskEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_task_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id', false, true);
            $table->integer('ceo_id', false, true);
            $table->string('proof_image_url');
            $table->string('remark');
            $table->boolean('is_sent_to_smm');
            $table->dateTime('created_at');
            $table->dateTime('submitted_at');
            $table->foreign('task_id')->references('id')->on('social_tasks')->onDelete('cascade');
            $table->foreign('ceo_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('social_task_entries');
    }
}
