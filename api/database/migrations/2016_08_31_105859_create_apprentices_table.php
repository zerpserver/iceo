<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprenticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apprentices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id', false, true);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('email');
            $table->integer('current_salary');
            $table->string('image');
            $table->integer('no_of_prospects');
            $table->integer('group_id', false, true);
            $table->integer('main_exit_strategy', false, true)->nullable();
            $table->integer('lead_strategy_1', false, true)->nullable();
            $table->integer('lead_strategy_2', false, true)->nullable();
            $table->integer('lead_strategy_3', false, true)->nullable();
            $table->text('notes');
            $table->dateTime('group_updated_at');
            $table->timestamps();
            $table->foreign('account_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('group_id')->references('id')->on('apprentices_groups')->onDelete('cascade');
            $table->foreign('main_exit_strategy')->references('id')->on('exit_strategies')->onDelete('set null');
            $table->foreign('lead_strategy_1')->references('id')->on('lead_strategies')->onDelete('set null');
            $table->foreign('lead_strategy_2')->references('id')->on('lead_strategies')->onDelete('set null');
            $table->foreign('lead_strategy_3')->references('id')->on('lead_strategies')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apprentices');
    }
}
