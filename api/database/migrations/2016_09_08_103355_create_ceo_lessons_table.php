<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCeoLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ceo_lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id', false, true);
            $table->string('title');
            $table->string('document');
            $table->integer('group_id', false, true);
            $table->timestamps();
            $table->foreign('account_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('group_id')->references('id')->on('ceo_lesson_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ceo_lessons');
    }
}
