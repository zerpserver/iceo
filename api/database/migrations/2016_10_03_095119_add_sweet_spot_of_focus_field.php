<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSweetSpotOfFocusField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apprentices', function (Blueprint $table) {
            $table->string('sweet_spots_of_focus')->after('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apprentices', function (Blueprint $table) {
            $table->dropColumn('sweet_spots_of_focus');
        });
    }
}
