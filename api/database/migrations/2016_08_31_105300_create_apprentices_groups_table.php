<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprenticesGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apprentices_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id', false, true);
            $table->string('title');
            $table->boolean('can_remove')->default(true);
            $table->boolean('count_as_lost')->nullable()->default(null);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->foreign('account_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apprentices_groups');
    }
}
