<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprenticesGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apprentices_goals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apprentice_id', false, true);
            $table->string('title');
            $table->boolean('status');
            $table->dateTime('created_at');
            $table->dateTime('completed_at');
            $table->foreign('apprentice_id')->references('id')->on('apprentices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apprentices_goals');
    }
}
