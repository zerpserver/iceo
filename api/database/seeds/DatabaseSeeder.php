<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->command->info("Users table seeded!");

        $this->call(SocialTasksSeeder::class);
        $this->command->info("Social Tasks table seeded!");

        $this->call(ApprenticesGroupsSeeder::class);
        $this->command->info("Apprentices Groups table seeded!");

        $this->call(ExitStrategiesSeeder::class);
        $this->command->info("Exit Strategies table seeded!");

        $this->call(LeadStrategiesSeeder::class);
        $this->command->info("Lead Strategies table seeded!");

        $this->call(ApprenticesSeeder::class);
        $this->command->info("Apprentices table seeded!");

        $this->call(ApprenticesContractsSeeder::class);
        $this->command->info("Apprentices Contracts table seeded!");

        $this->call(ApprenticesGoalsSeeder::class);
        $this->command->info("Apprentices Goals table seeded!");

        $this->call(CeoGoalsSeeder::class);
        $this->command->info("CEO Goals table seeded!");

        $this->call(CeoNotesSeeder::class);
        $this->command->info("CEO Notes table seeded!");

        Model::reguard();
    }
}
