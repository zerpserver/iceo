<?php

use Illuminate\Database\Seeder;
use App\ApprenticeGroup;

class ApprenticesGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $len = 10;

        while ($len){

            ApprenticeGroup::create([
                'account_id' => 1,
                'title' => $faker->company,
                'created_at' => $faker->dateTime,
                'updated_at' => $faker->dateTime
            ]);

            $len--;
        }

    }
}
