<?php

use Illuminate\Database\Seeder;
use \App\Apprentice;
use \App\ApprenticeContract;
use Carbon\Carbon;

class ApprenticesContractsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (Apprentice::all('id') as $apprentice){

            $len = rand(5, 10);

            while ($len){

                $now = Carbon::now();
                $status = rand(0, 2);
                $start = $faker->dateTimeBetween('-1 year', '-6 month')->format('Y-m-d H:i:s');
                $end = $faker->dateTimeBetween('-6 month', '+6 month')->format('Y-m-d H:i:s');
                $profit_amount = rand(1000, 10000);

                ApprenticeContract::create([
                    'apprentice_id' => $apprentice->id,
                    'seller_name' => $faker->userName,
                    'address' => $faker->streetAddress,
                    'city' => $faker->city,
                    'state' => $faker->citySuffix,
                    'zip' => 12345,
                    'exit_strategy_id' => rand(1, 3),
                    'profit_amount' => $profit_amount,
                    'apprentice_profit' => rand(100, $profit_amount),
                    'date_of_contract' => $start,
                    'contract_expiration' => $end,
                    'status' => $status,
                    'completed_at' => $status ? $end : '',
                    'created_at' => $start,
                    'updated_at' => $now
                ]);

                $len--;
            }

        }
    }
}
