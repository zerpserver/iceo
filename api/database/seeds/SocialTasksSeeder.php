<?php

use Illuminate\Database\Seeder;
use App\SocialTask;

class SocialTasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tasks = [
            [
                "title" => "Did you sign up any “New” Apprentices this week?",
                "description" => "Take a starter pic of each",
                "support_multiple_entries" => true
            ],
            [
                "title" => "Did any Apprentice put their first deal under contract this week?",
                "description" => "Take a pic or video of their excitement for getting their first deal",
                "support_multiple_entries" => true
            ],
            [
                "title" => "Did any Apprentice put a new deal under contract this week?",
                "description" => "Take a pic or video of them being excited about another payday",
                "support_multiple_entries" => true
            ],
            [
                "title" => "Did any Apprentice close on a deal and make a profit this week?",
                "description" => "Take a pic of check/wire or a video of their excitement for doing a deal",
                "support_multiple_entries" => true
            ],
            [
                "title" => "Did any Apprentice meet or exceed a goal this week?",
                "description" => "Take a video or pic goal they just met or exceeded",
                "support_multiple_entries" => true
            ],
            [
                "title" => "Please take a video of CEO walking into & exiting a meeting this week",
                "description" => "Talk about the meeting and what the goal is for the meeting and another video on how it ended",
                "support_multiple_entries" => false
            ],
            [
                "title" => "Can you get an Apprentice testimonial on video this week",
                "description" => "Thoughts of the training, a meeting or CEO having an impact on their life",
                "support_multiple_entries" => true
            ],
            [
                "title" => "Do you have any personal shout outs to Apprentices?",
                "description" => "It’s time to brag about them",
                "support_multiple_entries" => true
            ],
            [
                "title" => "Did you receive any killer emails or text messages you can screenshot this week of proof of Apprentices doing deals?",
                "description" => "",
                "support_multiple_entries" => true
            ],
            [
                "title" => "Can you get some deal progress pics? ",
                "description" => "Before, during or After",
                "support_multiple_entries" => true
            ],
            [
                "title" => "Did you do any community activities with the group?",
                "description" => "If so please take a pic or video of you and the group giving a helping hand",
                "support_multiple_entries" => true
            ],
            [
                "title" => "Please take a group pic of your meetings?",
                "description" => "Get some action shots of Apprentices doing something or deep in thought but don’t make it weird",
                "support_multiple_entries" => true
            ],
            [
                "title" => "Did you do any group showings of Apprentices looking at deals?",
                "description" => "Take a pic or video of the entire group in action looking at new deals",
                "support_multiple_entries" => true
            ]
        ];

        foreach ($tasks as $task) {
            SocialTask::create($task);
        }

    }
}
