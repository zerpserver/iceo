<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        User::create([
            'name' => $faker->name,
            'email' => 'test@example.com',
            'password' => bcrypt('123456'),
            'is_admin' => true
        ]);

        $len = rand(5, 10);

        while ($len){

            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt($faker->password),
                'is_admin' => false,
                'active' => $faker->boolean
            ]);

            $len--;
        }
    }
}
