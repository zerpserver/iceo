<?php

use Illuminate\Database\Seeder;
use App\User;
use App\CeoGoal;
use Carbon\Carbon;

class CeoGoalsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (User::all('id') as $user)
        {
            $len = rand(5, 10);

            while ($len)
            {
                CeoGoal::create([
                    'user_id' => $user->id,
                    'title' => $faker->jobTitle,
                    'status' => 0,
                    'created_at' => Carbon::now(),
                    'completed_at' => Carbon::now()
                ]);

                $len--;
            }
        }
    }
}
