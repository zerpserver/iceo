<?php

use Illuminate\Database\Seeder;
use App\ApprenticeGoal;
use App\Apprentice;

class ApprenticesGoalsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (Apprentice::all('id') as $apprentice){

            $len = rand(5, 10);

            while ($len){

                ApprenticeGoal::create([
                    'apprentice_id' => $apprentice->id,
                    'title' => $faker->jobTitle,
                    'status' => $faker->boolean,
                    'created_at' => $faker->dateTime,
                    'completed_at' => $faker->dateTime
                ]);

                $len--;
            }

        }

    }
}
