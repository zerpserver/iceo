<?php

use Illuminate\Database\Seeder;
use App\CeoNote;
use Carbon\Carbon;
use App\User;

class CeoNotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (User::all('id') as $user)
        {
            $len = rand(5, 10);

            while ($len)
            {
                CeoNote::create([
                    'user_id' => $user->id,
                    'note' => $faker->text(2000),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                $len--;
            }
        }
    }
}
