<?php

use Illuminate\Database\Seeder;
use App\Apprentice;
use App\ApprenticeGroup;
use App\ExitStrategy;
use App\LeadStrategy;
use Carbon\Carbon;

class ApprenticesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (ApprenticeGroup::all('id') as $apprenticeGroup)
        {
            $len = rand(5, 10);

            while ($len)
            {
                Apprentice::create([
                    'account_id' => 1,
                    'first_name' => $faker->firstName,
                    'last_name' => $faker->lastName,
                    'phone' => '1234567890',
                    'email' => $faker->email,
                    'current_salary' => (rand(1, 10) * 100),
                    'no_of_prospects' => $len,
                    'group_id' => $apprenticeGroup->id,
                    'notes' => $faker->text,
                    'main_exit_strategy' => ExitStrategy::all('id')->random(1)->id,
                    'lead_strategy_1' => LeadStrategy::all('id')->random(1)->id,
                    'lead_strategy_2' => LeadStrategy::all('id')->random(1)->id,
                    'lead_strategy_3' => LeadStrategy::all('id')->random(1)->id,
                    'created_at' => $faker->dateTimeBetween('-1 year'),
                    'updated_at' => Carbon::now()
                ]);

                $len--;
            }
        }

    }
}
