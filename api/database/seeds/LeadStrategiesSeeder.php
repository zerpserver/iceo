<?php

use Illuminate\Database\Seeder;
use App\LeadStrategy;

class LeadStrategiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Attorney',
            'Auction.com',
            'Bandit Sign',
            'Bank Owned',
            'Cold Call',
            'Craigslist',
            'Direct Mail',
            'Foreclosure Auction',
            'Friend/Family',
            'HUD Home',
            'MLS',
            'Project 100',
            'Short Sale',
            'Wholesaler',
            'Other'
        ];

        foreach ($data as $strategy){

            LeadStrategy::create([
                'title' => $strategy
            ]);

        }
    }
}
