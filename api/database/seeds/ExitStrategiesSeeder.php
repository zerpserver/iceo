<?php

use Illuminate\Database\Seeder;
use App\ExitStrategy;

class ExitStrategiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['Wholesale', 'Prehab', 'Rehab'];

        foreach ($data as $strategy){

            ExitStrategy::create([
                'title' => $strategy
            ]);

        }
    }
}
