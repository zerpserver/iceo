<?php

return [
    'social-proof'  => 'uploads/social-proof',
    'apprentices'   => 'uploads/apprentices',
    'calendar'      => 'uploads/calendar',
    'lessons'       => 'uploads/lessons'
];