<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CeoTarget extends Model
{
    protected $table = 'ceo_targets';

    protected $fillable = [
        'account_id', 'apprentice_target', 'deals_target', 'profit_target'
    ];

    protected $casts = [
        'account_id' => 'integer',
        'apprentice_target' => 'integer',
        'deals_target' => 'integer',
        'profit_target' => 'integer'
    ];

    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\User', 'account_id', 'id');
    }
}
