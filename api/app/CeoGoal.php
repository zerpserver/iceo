<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CeoGoal extends Model
{
    protected $table = 'ceo_goals';

    protected $fillable = [
        'user_id', 'title', 'status'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'status' => 'boolean'
    ];

    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
