<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprenticeProspectHistory extends Model
{
    protected $table = 'apprentice_prospect_history';

    protected $fillable = [
        'apprentice_id', 'prospect_count'
    ];

    protected $casts = [
        'apprentice_id' => 'integer',
        'prospect_count' => 'integer'
    ];

    public function apprentice(){
        return $this->belongsTo('App\Apprentice', 'apprentice_id', 'id');
    }
}
