<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadStrategy extends Model
{
    protected $table = 'lead_strategies';

    protected $fillable = [
        'title'
    ];

    protected $casts = [
        'account_id' => 'integer'
    ];

    public $timestamps = false;
}
