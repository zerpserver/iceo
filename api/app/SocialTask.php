<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialTask extends Model
{
    protected $table = 'social_tasks';

    protected $fillable = [
        'title', 'description', 'support_multiple_entries'
    ];

    protected $casts = [
        'support_multiple_entries' => 'boolean'
    ];

    public function entries(){
        return $this->hasMany('App\SocialTaskEntry', 'task_id', 'id');
    }
}
