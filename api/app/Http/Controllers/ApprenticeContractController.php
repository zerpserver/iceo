<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use JWTAuth;
use App\ApprenticeContract;

class ApprenticeContractController extends Controller
{
    public function lost($id)
    {
        $user = JWTAuth::parseToken()->toUser();

        $contract = ApprenticeContract::whereHas('apprentice', function ($query) use ($user) {
            $query->where('account_id', $user->id);
        })->find($id);

        if ($contract){

            $contract->status = 2;
            $contract->completed_at = Carbon::now();

            if ($contract->save()){

                return response()->json([
                    'success' => true,
                    'contract' => $contract
                ]);

            }
        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function closed($id)
    {
        $user = JWTAuth::parseToken()->toUser();

        $contract = ApprenticeContract::whereHas('apprentice', function ($query) use ($user) {
            $query->where('account_id', $user->id);
        })->find($id);

        if ($contract){

            $contract->status = 1;
            $contract->completed_at = Carbon::now();

            if ($contract->save()){

                return response()->json([
                    'success' => true,
                    'contract' => $contract
                ]);

            }
        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = JWTAuth::parseToken()->toUser();

        $contract = ApprenticeContract::whereHas('apprentice', function ($query) use ($user) {
            $query->where('account_id', $user->id);
        })->find($id);

        $contract->load('exit_strategy');

        return response()->json([
            'success' => true,
            'contract' => $contract
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $rules = [
            'apprentice_id' => 'required|exists:apprentices,id,account_id,'.$user->id,
            'seller_name' => 'required|between:3,25',
            'address' => 'required|between:3,128',
            'city' => 'required|between:3,128',
            'state' => 'required|between:3,64',
            'zip' => 'required|between:4,10',
            'exit_strategy_id' => 'required|exists:exit_strategies,id',
            'profit_amount' => 'required',
            'apprentice_profit' => 'required',
            'date_of_contract' => 'required|date_format:"m/d/Y"|date|before:contract_expiration',
            'contract_expiration' => 'required|date_format:"m/d/Y"|date|after:date_of_contract'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $apprenticeContract = new ApprenticeContract();

        $apprenticeContract->apprentice_id = $request->apprentice_id;
        $apprenticeContract->seller_name = $request->seller_name;
        $apprenticeContract->address = $request->address;
        $apprenticeContract->city = $request->city;
        $apprenticeContract->state = $request->state;
        $apprenticeContract->zip = $request->zip;
        $apprenticeContract->exit_strategy_id = $request->exit_strategy_id;
        $apprenticeContract->profit_amount = $request->profit_amount;
        $apprenticeContract->apprentice_profit = $request->apprentice_profit;
        $apprenticeContract->date_of_contract = $request->date_of_contract;
        $apprenticeContract->contract_expiration = $request->contract_expiration;

        if ($apprenticeContract->save()){

            return response()->json([
                'success' => true,
                'contract' => $apprenticeContract
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function update(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $rules = [
            'id' => 'required|exists:apprentice_contracts,id',
            'apprentice_id' => 'required|exists:apprentices,id,account_id,'.$user->id,
            'seller_name' => 'required|between:3,25',
            'address' => 'required|between:3,128',
            'city' => 'required|between:3,128',
            'state' => 'required|between:3,64',
            'zip' => 'required|between:4,10',
            'exit_strategy_id' => 'required|exists:exit_strategies,id',
            'profit_amount' => 'required',
            'apprentice_profit' => 'required',
            'date_of_contract' => 'required|date_format:"m/d/Y"|date|before:contract_expiration',
            'contract_expiration' => 'required|date_format:"m/d/Y"|date|after:date_of_contract',
            'status' => 'required|in:0,1,2'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $apprenticeContract = ApprenticeContract::find($request->id);

        if ($apprenticeContract){

            $apprenticeContract->apprentice_id = $request->apprentice_id;
            $apprenticeContract->seller_name = $request->seller_name;
            $apprenticeContract->address = $request->address;
            $apprenticeContract->city = $request->city;
            $apprenticeContract->state = $request->state;
            $apprenticeContract->zip = $request->zip;
            $apprenticeContract->exit_strategy_id = $request->exit_strategy_id;
            $apprenticeContract->profit_amount = $request->profit_amount;
            $apprenticeContract->apprentice_profit = $request->apprentice_profit;
            $apprenticeContract->date_of_contract = $request->date_of_contract;
            $apprenticeContract->contract_expiration = $request->contract_expiration;
            $apprenticeContract->status = $request->status;

            if ($apprenticeContract->save()){

                return response()->json([
                    'success' => true,
                    'contract' => $apprenticeContract
                ]);

            }

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
