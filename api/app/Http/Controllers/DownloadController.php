<?php

namespace App\Http\Controllers;

use File;

class DownloadController extends Controller
{
    public function index($path)
    {
        $publicPath = public_path($path);

        if (File::exists($publicPath) && File::isFile($publicPath)){

            return response()->download($publicPath);

        }

        abort(404);
    }
}
