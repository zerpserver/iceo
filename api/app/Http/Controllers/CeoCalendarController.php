<?php

namespace App\Http\Controllers;

use App\CeoCalendar;
use Illuminate\Http\Request;
use Carbon\Carbon;
use JWTAuth;
use Symfony\Component\HttpFoundation\Response;
use Validator;
use App\ApprenticeContract;

class CeoCalendarController extends Controller
{
    public function index()
    {
        $user = JWTAuth::parseToken()->toUser();

        $apprenticeContract = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->with('apprentice')->where('status', 0)->whereDate('contract_expiration', '>=', Carbon::now()->toDateString())->get();

        $events = CeoCalendar::where('account_id', $user->id)->whereDate('due_date', '>=', Carbon::now()->toDateString())->get();

        return response()->json([
            'success' => true,
            'contracts' => $apprenticeContract,
            'events' => $events
        ]);
    }

    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|string|between:3,255',
            'description' => 'required|string|max:2000',
            'due_date' => 'required|date|date_format:"m/d/Y"'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $ceoCalendar = new CeoCalendar();

        $ceoCalendar->account_id = $user->id;
        $ceoCalendar->title = $request->title;
        $ceoCalendar->description = $request->description;
        $ceoCalendar->due_date = $request->due_date;

        if ($ceoCalendar->save()){

            return response()->json([
                'success' => true,
                'event' => $ceoCalendar
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
