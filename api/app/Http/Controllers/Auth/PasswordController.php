<?php

namespace App\Http\Controllers\Auth;

use App\Events\ResetPassword;
use App\Http\Controllers\Controller;
use App\User;
use Symfony\Component\HttpFoundation\Response;
use Validator;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset(Request $request)
    {
        $rules = [
            'email' => 'required|email|max:255|exists:users,email,active,1',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = User::where('email', $request->email)->first();

        $password = str_random(8);

        $user->password = bcrypt($password);

        if ($user->save()){

            event(new ResetPassword(['user' => $user, 'password' => $password]));

            return response()->json([
                'success' => true
            ]);
        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
