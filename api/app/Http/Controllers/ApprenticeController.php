<?php

namespace App\Http\Controllers;

use App\ApprenticeContract;
use App\ApprenticeGoal;
use Illuminate\Http\Request;
use App\Apprentice;
use JWTAuth;
use File;
use Symfony\Component\HttpFoundation\Response;
use Validator;
use Carbon\Carbon;

class ApprenticeController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function goals($id)
    {
        $user = JWTAuth::parseToken()->toUser();

        $apprentice = Apprentice::where('account_id', $user->id)->with('goals')->find($id);

        if ($apprentice){

            return response()->json([
                'success' => true,
                'apprentice' => $apprentice
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public  function goal_add(Request $request, $id)
    {
        $rules = [
            'title' => 'required|string|between:3,255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $apprentice = Apprentice::where('account_id', $user->id)->find($id);

        if ($apprentice){

            $apprenticeGoal = new ApprenticeGoal();

            $apprenticeGoal->apprentice_id = $apprentice->id;
            $apprenticeGoal->title = $request->title;
            $apprenticeGoal->status = false;
            $apprenticeGoal->created_at = Carbon::now();

            if ($apprenticeGoal->save()){

                return response()->json([
                    'success' => true,
                    'goal' => $apprenticeGoal
                ]);
            }
        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function goal_update(Request $request, $id)
    {
        $rules = [
            'goal_id' => 'required|exists:apprentices_goals,id,apprentice_id,'.$id,
            'status' => 'required|accepted'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $apprenticeGoal = ApprenticeGoal::where('apprentice_id', $id)->whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->find($request->goal_id);

        if ($apprenticeGoal){

            $apprenticeGoal->status = $request->status;
            $apprenticeGoal->completed_at = Carbon::now();

            if ($apprenticeGoal->save()){

                return response()->json([
                    'success' => true
                ]);

            }

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function contracts($id)
    {
        $user = JWTAuth::parseToken()->toUser();

        $apprentice = Apprentice::where('account_id', $user->id)->with('contracts')->find($id);

        if ($apprentice){

            return response()->json([
                'success' => true,
                'apprentice' => $apprentice
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile($id)
    {
        $user = JWTAuth::parseToken()->toUser();

        $apprentice = Apprentice::where('account_id', $user->id)->with('calendar')->find($id);

        if ($apprentice){

            $this_year_profit = ApprenticeContract::where(['apprentice_id' => $apprentice->id, 'status' => 1])
                ->whereDate('completed_at', '>=', Carbon::now()->startOfYear()->toDateString())
                ->sum('apprentice_profit');

            $apprentice->percentage_this_year_profit = round(((int)$this_year_profit / $apprentice->current_salary) * 100);

            return response()->json([
                'success' => true,
                'apprentice' => $apprentice
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function scorecard($id)
    {
        $user = JWTAuth::parseToken()->toUser();

        $apprentice = Apprentice::where('account_id', $user->id)->find($id);

        if ($apprentice){

            $apprentice->under_contract = $apprentice->contracts()->where('status', 0)->count();
            $apprentice->deals_this_month = $apprentice->contracts()->where('status', 1)->whereDate('completed_at', '>=', Carbon::now()->startOfMonth()->toDateString())->count();
            $apprentice->deals_this_year = $apprentice->contracts()->where('status', 1)->whereDate('completed_at', '>=', Carbon::now()->startOfYear()->toDateString())->count();
            $apprentice->deals_lifetime = $apprentice->contracts()->where('status', 1)->count();
            $apprentice->lost_contracts = $apprentice->contracts()->where('status', 2)->count();

            return response()->json([
                'success' => true,
                'apprentice' => $apprentice
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rules = [
            'first_name' => 'required|between:3,15',
            'last_name' => 'required|between:3,15',
            'phone' => 'required|digits:10',
            'email' => 'required|email',
            'current_salary' => 'required|integer',
            'image' => $request->hasFile('image') ? 'image' : '',
            'no_of_prospects' => 'integer',
            'group_id' => 'required|exists:apprentices_groups,id'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $apprentice = new Apprentice();

        if ($request->hasFile('image')){

            $uploadPath = public_path(config('uploads.apprentices'));

            if (!File::exists($uploadPath)){
                File::makeDirectory($uploadPath, $mode = 0777, true, true);
            }

            $image = $request->file('image');

            $name = str_random(10).$image->getClientOriginalName();
            if ($image->move($uploadPath, $name)){
                $apprentice->image = $name;
            }

        }

        $apprentice->account_id = $user->id;
        $apprentice->first_name = $request->first_name;
        $apprentice->last_name = $request->last_name;
        $apprentice->phone = $request->phone;
        $apprentice->email = $request->email;
        $apprentice->current_salary = $request->current_salary;
        $apprentice->group_id = $request->group_id;

        if ($apprentice->save()){

            return response()->json([
                'success' => true,
                'apprentice' => $apprentice
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = JWTAuth::parseToken()->toUser();

        $apprentice = Apprentice::where('account_id', $user->id)->find($id);

        return response()->json([
            'success' => true,
            'apprentice' => $apprentice
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'first_name' => 'between:3,15',
            'last_name' => 'between:3,15',
            'phone' => 'digits:10',
            'email' => 'email',
            'current_salary' => 'integer',
            'image' => $request->hasFile('image') ? 'image' : '',
            'sweet_spots_of_focus' => 'string|max:255',
            'no_of_prospects' => 'integer',
            'group_id' => 'exists:apprentices_groups,id',
            'main_exit_strategy' => 'exists:exit_strategies,id',
            'lead_strategy_1' => 'exists:lead_strategies,id',
            'lead_strategy_2' => 'exists:lead_strategies,id',
            'lead_strategy_3' => 'exists:lead_strategies,id'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $apprentice = Apprentice::where('account_id', $user->id)->find($id);

        if ($apprentice){

            if ($request->has('first_name')){
                $apprentice->first_name = $request->first_name;
            }

            if ($request->has('last_name')){
                $apprentice->last_name = $request->last_name;
            }

            if ($request->has('phone')){
                $apprentice->phone = $request->phone;
            }

            if ($request->has('email')){
                $apprentice->email = $request->email;
            }

            if ($request->has('current_salary')){
                $apprentice->current_salary = $request->current_salary;
            }

            if ($request->hasFile('image')){

                $uploadPath = public_path() . '/uploads/apprentices';

                if (!File::exists($uploadPath)){
                    File::makeDirectory($uploadPath, $mode = 0777, true, true);
                }

                $image = $request->file('image');

                $name = str_random(10).$image->getClientOriginalName();
                if ($image->move($uploadPath, $name)){
                    $apprentice->image = $name;
                }

            }

            if ($request->has('sweet_spots_of_focus')){
                $apprentice->sweet_spots_of_focus = $request->sweet_spots_of_focus;
            }

            if ($request->has('no_of_prospects')){
                $apprentice->no_of_prospects = $request->no_of_prospects;
            }

            if ($request->has('group_id')){

                if ($apprentice->group_id != $request->group_id){

                    $apprentice->group_updated_at = Carbon::now();

                }

                $apprentice->group_id = $request->group_id;
            }

            if ($request->has('notes')){
                $apprentice->notes = e($request->notes);
            }

            if ($request->has('main_exit_strategy')){
                $apprentice->main_exit_strategy = $request->main_exit_strategy;
            }

            if ($request->has('lead_strategy_1')) {
                $apprentice->lead_strategy_1 = $request->lead_strategy_1;
            }

            if ($request->has('lead_strategy_2')) {
                $apprentice->lead_strategy_2 = $request->lead_strategy_2;
            }

            if ($request->has('lead_strategy_3')) {
                $apprentice->lead_strategy_3 = $request->lead_strategy_3;
            }

            if ($apprentice->save()){

                return response()->json([
                    'success' => true,
                    'apprentice' => $apprentice
                ]);
            }
        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $apprentice_ids = $request->apprentice_ids;

        if (! is_array($apprentice_ids)){
            $apprentice_ids = [$apprentice_ids];
        }

        $apprentice_ids = array_map('intval', $apprentice_ids);

        $user = JWTAuth::parseToken()->toUser();

        Apprentice::where('account_id', $user->id)->whereIn('id', $apprentice_ids)->get()->each(function($apprentice){
            $apprentice->delete();
        });

        return response()->json([
            'success' => true
        ]);
    }
}
