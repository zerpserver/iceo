<?php

namespace App\Http\Controllers;

use App\ExitStrategy;
use App\LeadStrategy;
use Illuminate\Http\Request;
use JWTAuth;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class StrategyController extends Controller
{
    public function exit_strategies()
    {
        return response()->json([
            'success' => true,
            'exit_strategies' => ExitStrategy::all()
        ]);
    }

    public function lead_strategies()
    {
        $user = JWTAuth::parseToken()->toUser();

        return response()->json([
            'success' => true,
            'lead_strategies' => LeadStrategy::where('account_id', $user->id)->orWhereNull('account_id')->get()
        ]);
    }

    public function create_lead_strategy(Request $request)
    {
        $rules = [
            'title' => 'required|string|between:3,255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $leadStrategy = new LeadStrategy();

        $leadStrategy->account_id = $user->id;
        $leadStrategy->title = $request->title;

        if ($leadStrategy->save()){

            return response()->json([
                'success' => true,
                'lead_strategy' => $leadStrategy
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
