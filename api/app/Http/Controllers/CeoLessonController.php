<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use File;
use Symfony\Component\HttpFoundation\Response;
use Validator;
use App\CeoLesson;

class CeoLessonController extends Controller
{
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|string|between:3,255',
            'file' => 'required|file|mimes:pdf,doc,docx,word,txt,ppt,pptx,key,xmind,zip',
            'group_id' => 'required|exists:ceo_lesson_groups,id'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $uploadPath = public_path(config('uploads.lessons')) . '/' . $user->id;

        if (!File::exists($uploadPath)){
            File::makeDirectory($uploadPath, $mode = 0777, true, true);
        }

        $document = $request->file('file');

        $name = str_random(10).$document->getClientOriginalName();

        if ($document->move($uploadPath, $name)){

            $ceoLesson = new CeoLesson();

            $ceoLesson->account_id = $user->id;
            $ceoLesson->title = $request->title;
            $ceoLesson->document = $name;
            $ceoLesson->group_id = $request->group_id;

            if ($ceoLesson->save()){

                return response()->json([
                    'success' => true,
                    'lesson' => $ceoLesson
                ]);

            }

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function update(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $rules = [
            'id' => 'required|exists:ceo_lessons,id,account_id,'.$user->id,
            'title' => 'required|string|between:3,255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $ceoLesson = CeoLesson::find($request->id);

        $ceoLesson->title = $request->title;

        if ($ceoLesson->save()){

            return response()->json([
                'success' => true,
                'lesson' => $ceoLesson
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function delete(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $rules = [
            'id' => 'required|exists:ceo_lessons,id,account_id,'.$user->id
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        if (CeoLesson::find($request->id)->delete()){

            return response()->json([
                'success' => true
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
