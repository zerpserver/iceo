<?php

namespace App\Http\Controllers;

use App\Apprentice;
use App\ApprenticeContract;
use App\CeoGoal;
use App\CeoNote;
use App\CeoTarget;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JWTAuth;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class CeoController extends Controller
{
    public function goals()
    {
        $user = JWTAuth::parseToken()->toUser();

        $user->load('target', 'goals');

        return response()->json([
            'success' => true,
            'ceo' => $user
        ]);
    }

    public function notes()
    {
        $user = JWTAuth::parseToken()->toUser();

        $user->load('notes');

        return response()->json([
            'success' => true,
            'ceo' => $user
        ]);
    }

    public function scoreboard()
    {
        $user = JWTAuth::parseToken()->toUser();

        $user->load('target');

        $user->apprentices = Apprentice::where('account_id', $user->id)->whereHas('group', function($query){
            $query->whereNull('count_as_lost')->orWhere('count_as_lost', 0);
        })->count();

        $user->deals = ApprenticeContract::where('status', 1)->whereHas('apprentice', function($query) use ($user){
            $query->where('account_id', $user->id);
        })->count();

        $user->profit_total = (float) ApprenticeContract::where('status', 1)->whereHas('apprentice', function($query) use ($user){
            $query->where('account_id', $user->id);
        })->sum('profit_amount');

        return response()->json([
            'success' => true,
            'ceo' => $user
        ]);
    }

    public function goal_add(Request $request)
    {
        $rules = [
            'title' => 'required|string|between:3,255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $ceoGoal = new CeoGoal();

        $ceoGoal->user_id = $user->id;
        $ceoGoal->title = $request->title;
        $ceoGoal->status = false;
        $ceoGoal->created_at = Carbon::now();

        if ($ceoGoal->save()){

            return response()->json([
                'success' => true,
                'goal' => $ceoGoal
            ]);
        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function goal_update(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $rules = [
            'id' => 'required|exists:ceo_goals,id,user_id,'.$user->id,
            'status' => 'required|accepted'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $ceoGoal = CeoGoal::find($request->id);

        if ($ceoGoal){

            $ceoGoal->status = $request->status;
            $ceoGoal->completed_at = Carbon::now();

            if ($ceoGoal->save()){

                return response()->json([
                    'success' => true
                ]);

            }

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_NOT_FOUND);
    }

    public function target_update(Request $request)
    {
        $rules = [
            'apprentice_target' => 'integer',
            'deals_target' => 'integer',
            'profit_target' => 'integer'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $ceoTarget = CeoTarget::where('account_id', $user->id)->first();

        if (empty($ceoTarget)){
            $ceoTarget = new CeoTarget();
        }

        $ceoTarget->account_id = $user->id;

        if ($request->has('apprentice_target')){
            $ceoTarget->apprentice_target = $request->apprentice_target;
        }

        if ($request->has('deals_target')){
            $ceoTarget->deals_target = $request->deals_target;
        }

        if ($request->has('profit_target')){
            $ceoTarget->profit_target = $request->profit_target;
        }

        if ($ceoTarget->save()){

            return response()->json([
                'success' => true,
                'target' => $ceoTarget
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function note_add(Request $request)
    {
        $rules = [
            'note' => 'required|string|max:2000'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $ceoNote = new CeoNote();

        $ceoNote->user_id = $user->id;
        $ceoNote->note = $request->note;

        if ($ceoNote->save()){

            return response()->json([
                'success' => true,
                'note' => $ceoNote
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function note_update(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $rules = [
            'id' => 'required|exists:ceo_notes,id,user_id,'.$user->id,
            'note' => 'required|string|max:2000'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $ceoNote = CeoNote::find($request->id);

        $ceoNote->note = $request->note;

        if ($ceoNote->save()){

            return response()->json([
                'success' => true,
                'note' => $ceoNote
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function note_delete(Request $request)
    {
        $note_ids = $request->note_ids;

        if (! is_array($note_ids)){
            $note_ids = [$note_ids];
        }

        $note_ids = array_map('intval', $note_ids);

        $user = JWTAuth::parseToken()->toUser();

        if (CeoNote::where('user_id', $user->id)->whereIn('id', $note_ids)->delete()){

            return response()->json([
                'success' => true
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
