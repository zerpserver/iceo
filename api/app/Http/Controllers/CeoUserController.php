<?php

namespace App\Http\Controllers;

use App\Events\SendUserWelcome;
use App\User;
use JWTAuth;
use Validator;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CeoUserController extends Controller
{
    public function index()
    {
        $user = JWTAuth::parseToken()->toUser();

        if ($user->is_admin){

            $users = User::where('id', '<>', $user->id)->get();

            return response()->json([
                'success' => true,
                'users' => $users
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_FORBIDDEN);
    }

    public function create(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        if (! $user->is_admin){

            return response()->json([
                'success' => false
            ], Response::HTTP_FORBIDDEN);

        }

        $rules = [
            'name' => 'required|string|between:3,255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $ceoUser = new User();

        $ceoUser->name = $request->name;
        $ceoUser->email = $request->email;
        $ceoUser->password = bcrypt($request->password);

        if ($ceoUser->save()){

            event(new SendUserWelcome($request->only('name', 'email', 'password')));

            $ceoUser = $ceoUser->fresh();

            return response()->json([
                'success' => true,
                'user' => $ceoUser
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function update(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        if (! $user->is_admin){

            return response()->json([
                'success' => false
            ], Response::HTTP_FORBIDDEN);

        }

        $rules = [
            'id' => 'required|integer|exists:users,id',
            'name' => 'string|between:3,255',
            'email' => 'email|max:255|unique:users,email,'.$request->id.',id',
            'password' => 'min:6|confirmed',
            'active' => 'boolean',
            'is_admin' => 'boolean'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        if ($ceoUser = User::find($request->id)){

            if ($request->has('name')){ $ceoUser->name = $request->name; }

            if ($request->has('email')){ $ceoUser->email = $request->email; }

            if ($request->has('password')){ $ceoUser->password = $request->password; }

            if ($request->has('active')){ $ceoUser->active = $request->active; }

            if ($request->has('is_admin')){ $ceoUser->is_admin = $request->is_admin; }

            if ($ceoUser->save()){

                return response()->json([
                    'success' => true,
                    'user' => $ceoUser
                ]);

            }

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function delete(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        if (! $user->is_admin){

            return response()->json([
                'success' => false
            ], Response::HTTP_FORBIDDEN);

        }

        $rules = [
            'id' => 'required|integer|exists:users,id'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        if (User::find($request->id)->delete())
        {
            return response()->json([
                'success' => true
            ]);
        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
