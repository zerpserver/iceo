<?php

namespace App\Http\Controllers;

use App\Apprentice;
use Illuminate\Http\Request;
use App\ApprenticeGroup;
use JWTAuth;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class ApprenticeGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = JWTAuth::parseToken()->toUser();

        $groups = ApprenticeGroup::where('account_id', $user->id)->withCount('apprentices')->get();

        return response()->json([
            'success' => true,
            'groups' => $groups
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|between:3,25',
            'count_as_lost' => 'boolean'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $apprenticeGroup = new ApprenticeGroup();

        $apprenticeGroup->account_id = $user->id;
        $apprenticeGroup->title = $request->title;

        if ($request->has('count_as_lost')){
            $apprenticeGroup->count_as_lost = (int) $request->count_as_lost;
        }

        if ($apprenticeGroup->save()){

            $apprenticeGroup = ApprenticeGroup::withCount('apprentices')->find($apprenticeGroup->id);

            return response()->json([
                'success' => true,
                'group' => $apprenticeGroup
            ]);
        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = JWTAuth::parseToken()->toUser();

        $group = ApprenticeGroup::where('account_id', $user->id)->with('apprentices')->find($id);

        return response()->json([
            'success' => true,
            'group' => $group
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $group_ids = $request->group_ids;

        if (! is_array($group_ids)){
            $group_ids = [$group_ids];
        }

        $group_ids = array_map('intval', $group_ids);

        $user = JWTAuth::parseToken()->toUser();

        ApprenticeGroup::where(['account_id' => $user->id, 'can_remove' => true])->whereIn('id', $group_ids)->get()->each(function ($apprenticeGroup){
            $apprenticeGroup->delete();
        });

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required|between:3,25'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $apprenticeGroup = ApprenticeGroup::where('account_id', $user->id)->withCount('apprentices')->find($id);

        if ($apprenticeGroup){
            $apprenticeGroup->title = $request->title;

            if ($apprenticeGroup->save()){
                return response()->json([
                    'success' => true,
                    'group' => $apprenticeGroup
                ]);
            }
        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function move(Request $request)
    {
        $rules = [
            'group_id' => 'required|integer|exists:apprentices_groups,id'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $apprentice_ids = $request->apprentice_ids;

        if (! is_array($apprentice_ids)){
            $apprentice_ids = [$apprentice_ids];
        }

        $apprentice_ids = array_map('intval', $apprentice_ids);

        $user = JWTAuth::parseToken()->toUser();

        if (Apprentice::where('account_id', $user->id)->whereIn('id', $apprentice_ids)->update(['group_id' => $request->group_id])){

            return response()->json([
                'success' => true
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
