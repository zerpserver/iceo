<?php

namespace App\Http\Controllers;

use App\CeoLessonGroup;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Validator;
use JWTAuth;

class CeoLessonGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = JWTAuth::parseToken()->toUser();

        $groups = CeoLessonGroup::where('account_id', $user->id)->withCount('lessons')->get();

        return response()->json([
            'success' => true,
            'groups' => $groups
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|string|between:3,255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $ceoLessonGroup = new CeoLessonGroup();

        $ceoLessonGroup->account_id = $user->id;
        $ceoLessonGroup->title = $request->title;

        if ($ceoLessonGroup->save()){

            $ceoLessonGroup->lessons_count = 0;

            return response()->json([
                'success' => true,
                'group' => $ceoLessonGroup
            ]);
        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = JWTAuth::parseToken()->toUser();

        $group = CeoLessonGroup::where('account_id', $user->id)->with('lessons')->find($id);

        return response()->json([
            'success' => true,
            'group' => $group
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $rules = [
            'id' => 'required|integer|exists:ceo_lesson_groups,id,account_id,' . $user->id
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $ceoLessonGroup = CeoLessonGroup::where('account_id', $user->id)->find($request->id);

        if ($ceoLessonGroup){

            return response()->json([
                'success' => $ceoLessonGroup->delete()
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $rules = [
            'id' => 'required|integer|exists:ceo_lesson_groups,id,account_id,' . $user->id,
            'title' => 'required|string|between:3,255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $ceoLessonGroup = CeoLessonGroup::where('account_id', $user->id)->withCount('lessons')->find($request->id);

        if ($ceoLessonGroup){
            $ceoLessonGroup->title = $request->title;

            if ($ceoLessonGroup->save()){

                return response()->json([
                    'success' => true,
                    'group' => $ceoLessonGroup
                ]);

            }
        }

        return response()->json([
            'success' => false
        ], Response::HTTP_NOT_FOUND);
    }
}
