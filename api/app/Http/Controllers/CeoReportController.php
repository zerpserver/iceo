<?php

namespace App\Http\Controllers;

use App\Apprentice;
use App\ApprenticeContract;
use App\CeoTarget;
use Carbon\Carbon;
use JWTAuth;

class CeoReportController extends Controller
{
    public function apprentices()
    {
        $user = JWTAuth::parseToken()->toUser();

        $report = [];

        $report['new_week'] = Apprentice::where('account_id', $user->id)->whereHas('group', function ($query){
            $query->whereNull('count_as_lost')->orWhere('count_as_lost', 0);
        })->whereDate('created_at', '>=', Carbon::now()->subWeek()->toDateTimeString())->count();

        $report['new_month'] = Apprentice::where('account_id', $user->id)->whereHas('group', function ($query){
            $query->whereNull('count_as_lost')->orWhere('count_as_lost', 0);
        })->whereDate('created_at', '>=', Carbon::now()->subMonth()->toDateTimeString())->count();

        $report['new_year'] = Apprentice::where('account_id', $user->id)->whereHas('group', function ($query){
            $query->whereNull('count_as_lost')->orWhere('count_as_lost', 0);
        })->where('created_at', '>=', Carbon::now()->subYear()->toDateTimeString())->count();

        $report['lost_week'] = Apprentice::where('account_id', $user->id)->whereHas('group', function ($query){
            $query->where('count_as_lost', 1);
        })->whereDate('group_updated_at', '>=', Carbon::now()->subWeek()->toDateTimeString())->count();

        $report['lost_month'] = Apprentice::where('account_id', $user->id)->whereHas('group', function ($query){
            $query->where('count_as_lost', 1);
        })->whereDate('group_updated_at', '>=', Carbon::now()->subMonth()->toDateTimeString())->count();

        $report['lost_year'] = Apprentice::where('account_id', $user->id)->whereHas('group', function ($query){
            $query->where('count_as_lost', 1);
        })->whereDate('group_updated_at', '>=', Carbon::now()->subYear()->toDateTimeString())->count();

        $apprentices = Apprentice::where('account_id', $user->id)->whereHas('group', function ($query){
            $query->whereNull('count_as_lost')->orWhere('count_as_lost', 0);
        })->get()->each(function ($apprentice){
            $apprentice->profit_contracts = (float) ApprenticeContract::where(['apprentice_id' => $apprentice->id, 'status' => 1])->sum('apprentice_profit');
            $apprentice->closed_contracts = ApprenticeContract::where(['apprentice_id' => $apprentice->id, 'status' => 1])->count();
            $apprentice->lost_contracts = ApprenticeContract::where(['apprentice_id' => $apprentice->id, 'status' => 2])->count();
        });

        $user->report_apprentices = $report;
        $user->apprentices = $apprentices;

        return response()->json([
            'success' => true,
            'ceo' => $user
        ]);
    }

    public function financial()
    {
        $user = JWTAuth::parseToken()->toUser();

        $report = [];

        $report['profit_last_month'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 1)->whereDate('completed_at', '>=', Carbon::now()->lastOfMonth()->startOfMonth()->toDateTimeString())
            ->whereDate('completed_at', '<=', Carbon::now()->lastOfMonth()->endOfMonth()->toDateTimeString())
            ->sum('profit_amount');

        $report['profit_this_month'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 1)->whereDate('completed_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->sum('profit_amount');

        $report['profit_this_year'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 1)->whereDate('completed_at', '>=', Carbon::now()->startOfYear()->toDateTimeString())->sum('profit_amount');

        $report['profit_lifetime'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 1)->sum('profit_amount');

        $report['potential_next_month'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 0)->whereDate('contract_expiration', '>=', Carbon::now()->addMonth()->startOfMonth())
            ->whereDate('contract_expiration', '<=', Carbon::now()->addMonth()->endOfMonth())
            ->sum('profit_amount');

        $report['potential_lost'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 0)->whereDate('contract_expiration', '<', Carbon::now()->toDateTimeString())->sum('profit_amount');

        $report['avg_per_dial'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 1)->avg('profit_amount');

        $report['avg_per_wholesale'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 1)->whereHas('exit_strategy', function($query){
            $query->where('title', 'Wholesale');
        })->avg('profit_amount');

        $report['avg_per_prehab'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 1)->whereHas('exit_strategy', function($query){
            $query->where('title', 'Prehab');
        })->avg('profit_amount');

        $report['avg_per_rehab'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 1)->whereHas('exit_strategy', function($query){
            $query->where('title', 'Rehab');
        })->avg('profit_amount');

        $user->financial = $report;

        return response()->json([
            'success' => true,
            'ceo' => $user
        ]);
    }

    public function deals()
    {
        $user = JWTAuth::parseToken()->toUser();

        $report = [];

        $report['current_total_prospects'] = Apprentice::where('account_id', $user->id)->sum('no_of_prospects');

        $report['current_under_contract'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 0)->count();

        $ceoTarget = CeoTarget::where('account_id', $user->id)->first();

        if ($ceoTarget && $ceoTarget->deals_target > 0){
            $report['goal_closed_this_month'] = round($ceoTarget->deals_target / 12);
        }
        else{
            $report['goal_closed_this_month'] = 0;
        }

        $report['total_closed_this_month'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 1)->whereDate('completed_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->count();

        $report['total_closed_this_year'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 1)->whereDate('completed_at', '>=', Carbon::now()->startOfYear()->toDateTimeString())->count();

        $report['total_closed_lifetime'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 1)->count();

        $report['total_lost_this_month'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 2)->whereDate('completed_at', '>=', Carbon::now()->startOfMonth()->toDateTimeString())->count();

        $report['total_lost_this_year'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 2)->whereDate('completed_at', '>=', Carbon::now()->startOfYear()->toDateTimeString())->count();

        $report['total_lost_lifetime'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->where('status', 2)->count();

        $report['contracts'] = ApprenticeContract::whereHas('apprentice', function ($query) use ($user){
            $query->where('account_id', $user->id);
        })->with(['apprentice' => function($query){
            $query->select(['id', 'first_name', 'last_name']);
        }])->where('status', 0)->whereDate('contract_expiration', '>=', Carbon::now()->toDateTimeString())->get();

        $user->deals = $report;

        return response()->json([
            'success' => true,
            'ceo' => $user
        ]);
    }
}
