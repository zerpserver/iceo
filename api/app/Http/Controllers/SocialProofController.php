<?php

namespace App\Http\Controllers;

use App\Events\SendSocialProof;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\SocialTask;
use App\SocialTaskEntry;
use Symfony\Component\HttpFoundation\Response;
use Validator;
use JWTAuth;
use File;

class SocialProofController extends Controller
{
    public function index()
    {
        $user = JWTAuth::parseToken()->toUser();

        $tasks = SocialTask::with(['entries' => function($query) use ($user) {
            $query->where(['is_sent_to_smm' => 0, 'ceo_id' => $user->id]);
        }])->get();

        return response()->json([
            'success' => true,
            'tasks' => $tasks,
            'social_media_manager_email' => $user->social_media_manager_email
        ]);
    }

    public function show($id)
    {
        $user = JWTAuth::parseToken()->toUser();

        $task = SocialTask::with(['entries' => function($query) use ($user) {
            $query->where(['is_sent_to_smm' => 0, 'ceo_id' => $user->id]);
        }])->find($id);

        if ($task){

            return response()->json([
                'success' => true,
                'task' => $task
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function send(Request $request)
    {
        $rules = [
            'email' => 'required|email|max:255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $user->social_media_manager_email = $request->email;

        if ($user->save()){

            $tasks = SocialTask::whereHas('entries', function ($query) use ($user){
                $query->where(['is_sent_to_smm' => 0, 'ceo_id' => $user->id]);
            })->with(['entries' => function($query){
                $query->where('is_sent_to_smm', 0);
            }])->get(['id', 'title']);

            if (SocialTaskEntry::where(['is_sent_to_smm' => 0, 'ceo_id' => $user->id])->update(['is_sent_to_smm' => 1]))
            {
                event(new SendSocialProof(['tasks' => $tasks]));

                return response()->json([
                    'success' => true
                ]);
            }

        }

        return response()->json([
            'success' => false
        ]);
    }

    public function delete(Request $request)
    {
        $entry_ids = $request->entry_ids;

        if (! is_array($entry_ids)){
            $entry_ids = [$entry_ids];
        }

        $entry_ids = array_map('intval', $entry_ids);

        $user = JWTAuth::parseToken()->toUser();

        SocialTaskEntry::where('ceo_id', $user->id)->whereIn('id', $entry_ids)->get()->each(function ($socialTaskEntry){
            $socialTaskEntry->delete();
        });

        return response()->json([
            'success' => true
        ]);
    }

    public function create(Request $request)
    {
        $rules = [
            'task_id' => 'required|exists:social_tasks,id'
        ];

        $files = $request->file('files');

        if (is_array($files)){
            $nbr = count($files) - 1;

            foreach (range(0, $nbr) as $index){
                $rules['files.' . $index] = 'required|file|mimetypes:image/jpeg,image/png,image/gif,image/bmp,image/svg+xml,video/mp4,video/webm,video/ogg|max:100000';
            }
        }
        else{
            $rules['files'] = 'required|file|mimetypes:image/jpeg,image/png,image/gif,image/bmp,image/svg+xml,video/mp4,video/webm,video/ogg|max:100000';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $user = JWTAuth::parseToken()->toUser();

        $uploadPath = public_path(config('uploads.social-proof')) . '/' . $user->id;

        if (!File::exists($uploadPath)){
            File::makeDirectory($uploadPath, $mode = 0777, true, true);
        }

        $files = is_array($files) ? $files : [$files];

        foreach ($files as $file){
            $name = str_random(10).$file->getClientOriginalName();
            if ($file->move($uploadPath, $name)){

                $socialTaskEntry = new SocialTaskEntry();

                $socialTaskEntry->task_id = $request->task_id;
                $socialTaskEntry->ceo_id = $user->id;
                $socialTaskEntry->proof_image_url = $name;
                $socialTaskEntry->is_sent_to_smm = false;

                $date = Carbon::now()->toDateTimeString();

                $socialTaskEntry->created_at = $date;
                $socialTaskEntry->submitted_at = $date;

                $socialTaskEntry->save();

            }
        }

        $entries = SocialTask::find($request->task_id)->entries()->where('is_sent_to_smm', 0)->get();

        return response()->json([
            'success' => true,
            'entries' => $entries
        ]);
    }

    public function update(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $rules = [
            'id' => 'required|exists:social_task_entries,id,ceo_id,'.$user->id,
            'remark' => 'string|max:255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $socialTaskEntry = SocialTaskEntry::where('ceo_id', $user->id)->find($request->id);

        $socialTaskEntry->remark = $request->remark;

        if ($socialTaskEntry->save()){

            return response()->json([
                'success' => true,
                'entry' => $socialTaskEntry
            ]);

        }

        return response()->json([
            'success' => false
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
