<?php

namespace App\Http\Controllers;

use App\ApprenticeCalendar;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use JWTAuth;
use Validator;
use File;

class ApprenticeCalendarController extends Controller
{
    public function delete(Request $request)
    {
        $calendar_ids = $request->calendar_ids;

        if (! is_array($calendar_ids)){
            $calendar_ids = [$calendar_ids];
        }

        $calendar_ids = array_map('intval', $calendar_ids);

        $user = JWTAuth::parseToken()->toUser();

        ApprenticeCalendar::whereHas('apprentice', function($query) use ($user){

            $query->where('account_id', $user->id);

        })->whereIn('id', $calendar_ids)->get()->each(function($apprenticeCalendar){

            $apprenticeCalendar->delete();

        });

        return response()->json([
            'success' => true
        ]);
    }

    public function create(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $rules = [
            'apprentice_id' => 'required|exists:apprentices,id,account_id,'.$user->id
        ];

        $images = $request->file('images');

        if (is_array($images)){
            $nbr = count($images) - 1;

            foreach (range(0, $nbr) as $index){
                $rules['images.' . $index] = 'required|image';
            }
        }
        else{
            $rules['images'] = 'required|image';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $this->formatValidationErrors($validator)
            ]);
        }

        $uploadPath = public_path(config('uploads.calendar')) . '/' . $request->apprentice_id;

        if (!File::exists($uploadPath)){
            File::makeDirectory($uploadPath, $mode = 0777, true, true);
        }

        $images = is_array($images) ? $images : [$images];

        $calendar = new Collection();

        foreach ($images as $image){
            $name = str_random(10).$image->getClientOriginalName();
            if ($image->move($uploadPath, $name)){

                $apprenticeCalendar = new ApprenticeCalendar();

                $apprenticeCalendar->apprentice_id = $request->apprentice_id;
                $apprenticeCalendar->image = $name;

                if ($apprenticeCalendar->save()){
                    $calendar->add($apprenticeCalendar);
                }
            }
        }

        return response()->json([
            'success' => true,
            'calendar' => $calendar
        ]);
    }
}
