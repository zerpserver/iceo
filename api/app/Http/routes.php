<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'auth'], function() {
    Route::post('signup', '\App\Http\Controllers\Auth\AuthController@signup');
    Route::post('signin', '\App\Http\Controllers\Auth\AuthController@signin');
    Route::post('forgot', '\App\Http\Controllers\Auth\PasswordController@reset');
    Route::get('refresh', ['middleware' => 'jwt.refresh', function () {}]);
});

Route::group(['prefix' => 'api/v1', 'middleware' => 'jwt.auth'], function () {

    Route::get('social-proofs', '\App\Http\Controllers\SocialProofController@index');
    Route::post('social-proofs/send', '\App\Http\Controllers\SocialProofController@send');
    Route::post('social-proofs/create', '\App\Http\Controllers\SocialProofController@create');
    Route::post('social-proofs/update', '\App\Http\Controllers\SocialProofController@update');
    Route::post('social-proofs/delete', '\App\Http\Controllers\SocialProofController@delete');
    Route::get('social-proofs/{id}', '\App\Http\Controllers\SocialProofController@show');

    Route::get('groups/list', '\App\Http\Controllers\ApprenticeGroupController@index');
    Route::get('groups/{id}', '\App\Http\Controllers\ApprenticeGroupController@show');
    Route::post('groups/add', '\App\Http\Controllers\ApprenticeGroupController@create');
    Route::post('groups/delete', '\App\Http\Controllers\ApprenticeGroupController@delete');
    Route::post('groups/move', '\App\Http\Controllers\ApprenticeGroupController@move');
    Route::post('groups/{id}/edit', '\App\Http\Controllers\ApprenticeGroupController@update');

    Route::get('apprentices/{id}', '\App\Http\Controllers\ApprenticeController@show');
    Route::get('apprentices/{id}/contracts', '\App\Http\Controllers\ApprenticeController@contracts');
    Route::get('apprentices/{id}/goals', '\App\Http\Controllers\ApprenticeController@goals');
    Route::post('apprentices/{id}/goals/add', '\App\Http\Controllers\ApprenticeController@goal_add');
    Route::post('apprentices/{id}/goals/edit', '\App\Http\Controllers\ApprenticeController@goal_update');
    Route::get('apprentices/{id}/profile', '\App\Http\Controllers\ApprenticeController@profile');
    Route::get('apprentices/{id}/scorecard', '\App\Http\Controllers\ApprenticeController@scorecard');
    Route::post('apprentices/add', '\App\Http\Controllers\ApprenticeController@create');
    Route::post('apprentices/{id}/edit', '\App\Http\Controllers\ApprenticeController@update');
    Route::post('apprentices/delete', '\App\Http\Controllers\ApprenticeController@delete');

    Route::get('strategies/lead', '\App\Http\Controllers\StrategyController@lead_strategies');
    Route::get('strategies/exit', '\App\Http\Controllers\StrategyController@exit_strategies');
    Route::post('strategies/lead/add', '\App\Http\Controllers\StrategyController@create_lead_strategy');

    Route::post('contracts/add', '\App\Http\Controllers\ApprenticeContractController@create');
    Route::post('contracts/edit', '\App\Http\Controllers\ApprenticeContractController@update');
    Route::get('contracts/{id}', '\App\Http\Controllers\ApprenticeContractController@show');
    Route::get('contracts/{id}/lost', '\App\Http\Controllers\ApprenticeContractController@lost');
    Route::get('contracts/{id}/closed', '\App\Http\Controllers\ApprenticeContractController@closed');

    Route::post('calendar/add', '\App\Http\Controllers\ApprenticeCalendarController@create');
    Route::post('calendar/delete', '\App\Http\Controllers\ApprenticeCalendarController@delete');

    Route::get('ceo/goals', '\App\Http\Controllers\CeoController@goals');
    Route::get('ceo/notes', '\App\Http\Controllers\CeoController@notes');
    Route::post('ceo/goals/add', '\App\Http\Controllers\CeoController@goal_add');
    Route::get('ceo/scoreboard', '\App\Http\Controllers\CeoController@scoreboard');
    Route::post('ceo/notes/add', '\App\Http\Controllers\CeoController@note_add');
    Route::post('ceo/notes/edit', '\App\Http\Controllers\CeoController@note_update');
    Route::post('ceo/notes/delete', '\App\Http\Controllers\CeoController@note_delete');
    Route::post('ceo/update/target', '\App\Http\Controllers\CeoController@target_update');
    Route::post('ceo/update/goal', '\App\Http\Controllers\CeoController@goal_update');

    Route::get('ceo/lesson-groups', '\App\Http\Controllers\CeoLessonGroupController@index');
    Route::get('ceo/lesson-groups/{id}', '\App\Http\Controllers\CeoLessonGroupController@show');
    Route::post('ceo/lesson-groups/add', '\App\Http\Controllers\CeoLessonGroupController@create');
    Route::post('ceo/lesson-groups/edit', '\App\Http\Controllers\CeoLessonGroupController@update');
    Route::post('ceo/lesson-groups/delete', '\App\Http\Controllers\CeoLessonGroupController@delete');

    Route::post('ceo/lessons/add', '\App\Http\Controllers\CeoLessonController@create');
    Route::post('ceo/lessons/edit', '\App\Http\Controllers\CeoLessonController@update');
    Route::post('ceo/lessons/delete', '\App\Http\Controllers\CeoLessonController@delete');

    Route::get('ceo/report/apprentices', '\App\Http\Controllers\CeoReportController@apprentices');
    Route::get('ceo/report/deals', '\App\Http\Controllers\CeoReportController@deals');
    Route::get('ceo/report/financial', '\App\Http\Controllers\CeoReportController@financial');

    Route::get('ceo/calendar', '\App\Http\Controllers\CeoCalendarController@index');
    Route::post('ceo/calendar/add', '\App\Http\Controllers\CeoCalendarController@create');

    Route::get('ceo/users', '\App\Http\Controllers\CeoUserController@index');
    Route::post('ceo/users/add', '\App\Http\Controllers\CeoUserController@create');
    Route::post('ceo/users/edit', '\App\Http\Controllers\CeoUserController@update');
    Route::post('ceo/users/delete', '\App\Http\Controllers\CeoUserController@delete');
});

Route::get('download/{path}', '\App\Http\Controllers\DownloadController@index')->where(['path' => '.+']);