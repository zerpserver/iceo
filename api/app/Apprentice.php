<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;

class Apprentice extends Model
{
    protected $table = 'apprentices';

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'current_salary',
        'image',
        'no_of_prospects',
        'notes',
        'group_id',
        'main_exit_strategy',
        'lead_strategy_1',
        'lead_strategy_2',
        'lead_strategy_3'
    ];

    protected $casts = [
        'account_id' => 'integer',
        'current_salary' => 'integer',
        'no_of_prospects' => 'integer',
        'group_id' => 'integer',
        'main_exit_strategy' => 'integer',
        'lead_strategy_1' => 'integer',
        'lead_strategy_2' => 'integer',
        'lead_strategy_3' => 'integer'
    ];

    protected $dates = ['group_updated_at'];

    protected static function boot()
    {
        parent::boot();

        Apprentice::updated(function($apprentice){

            $no_of_prospects = intval($apprentice->getOriginal('no_of_prospects'));

            if ($apprentice->no_of_prospects != $no_of_prospects)
            {
                $prospectHistory = new ApprenticeProspectHistory();

                $prospectHistory->apprentice_id = $apprentice->id;
                $prospectHistory->prospect_count = $no_of_prospects;

                $prospectHistory->save();
            }

        });

        Apprentice::deleting(function($apprentice){

            $apprentice->contracts()->get()->each(function($contract){
                $contract->delete();
            });

            $apprentice->goals()->get()->each(function($goal){
                $goal->delete();
            });

            $apprentice->calendar()->get()->each(function($calendar){
                $calendar->delete();
            });

        });

        Apprentice::deleted(function($apprentice){

            if (! empty($apprentice->attributes['image']))
            {
                $path       = config('uploads.apprentices') . '/' . $apprentice->attributes['image'];
                $publicPath = public_path($path);

                if (File::exists($publicPath)){

                    File::delete($publicPath);

                }
            }

        });
    }

    public function getImageAttribute($value)
    {

        $path       = config('uploads.apprentices') . '/' . $value;

        $publicPath = public_path($path);
        $url        = url($path);

        if (! empty($value) && File::exists($publicPath)){
            return $url;
        }

        return '';

    }

    public function contracts()
    {
        return $this->hasMany('App\ApprenticeContract', 'apprentice_id', 'id');
    }

    public function goals()
    {
        return $this->hasMany('App\ApprenticeGoal', 'apprentice_id', 'id');
    }

    public function calendar()
    {
        return $this->hasMany('App\ApprenticeCalendar', 'apprentice_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo('App\ApprenticeGroup', 'group_id', 'id');
    }

}
