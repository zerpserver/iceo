<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ApprenticeContract extends Model
{
    protected $table = 'apprentice_contracts';

    protected $fillable = [
        'apprentice_id',
        'seller_name',
        'address',
        'city',
        'state',
        'zip',
        'exit_strategy_id',
        'profit_amount',
        'date_of_contract',
        'contract_expiration',
        'status'
    ];

    protected $casts = [
        'apprentice_id' => 'integer',
        'exit_strategy_id' => 'integer',
        'status' => 'integer'
    ];

    protected $dates = ['date_of_contract', 'contract_expiration'];

    public function setDateOfContractAttribute($date){
        $this->attributes['date_of_contract'] = Carbon::parse($date)->format('Y-m-d');
    }

    public function setContractExpirationAttribute($date){
        $this->attributes['contract_expiration'] = Carbon::parse($date)->format('Y-m-d');
    }

    public function getDateOfContractAttribute($date){
        return $this->attributes['date_of_contract'] = Carbon::parse($date)->format('m/d/Y');
    }

    public function getContractExpirationAttribute($date){
        return $this->attributes['contract_expiration'] = Carbon::parse($date)->format('m/d/Y');
    }

    public function apprentice(){
        return $this->belongsTo('App\Apprentice', 'apprentice_id', 'id');
    }

    public function exit_strategy(){
        return $this->hasOne('App\ExitStrategy', 'id', 'exit_strategy_id');
    }
}
