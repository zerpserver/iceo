<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;

class CeoLesson extends Model
{
    protected $table = 'ceo_lessons';

    protected $fillable = [
        'account_id', 'title', 'document', 'group_id'
    ];

    protected $casts = [
        'account_id' => 'integer',
        'group_id' => 'integer'
    ];

    protected static function boot()
    {
        parent::boot();

        CeoLesson::deleted(function($ceoLesson){

            if (! empty($ceoLesson->attributes['document']))
            {
                $path       = config('uploads.lessons') .'/'. $ceoLesson->account_id . '/' . $ceoLesson->attributes['document'];
                $publicPath = public_path($path);

                if (File::exists($publicPath)){

                    File::delete($publicPath);

                }
            }

        });

    }

    public function getDocumentAttribute($value)
    {
        $path       = config('uploads.lessons') .'/'. $this->account_id . '/' . $value;

        $publicPath = public_path($path);
        $url        = url($path);

        if (! empty($value) && File::exists($publicPath)){
            return $url;
        }

        return '';

    }

    public function user()
    {
        return $this->belongsTo('App\User', 'account_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo('App\CeoLessonGroup', 'group_id', 'id');
    }
}
