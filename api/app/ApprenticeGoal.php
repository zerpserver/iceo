<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprenticeGoal extends Model
{
    protected $table = 'apprentices_goals';

    protected $fillable = [
        'apprentice_id', 'title', 'status'
    ];

    protected $casts = [
        'apprentice_id' => 'integer',
        'status' => 'boolean'
    ];

    public $timestamps = false;

    public function apprentice(){
        return $this->belongsTo('App\Apprentice', 'apprentice_id', 'id');
    }
}
