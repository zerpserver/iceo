<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;

class SocialTaskEntry extends Model
{
    protected $table = 'social_task_entries';

    public $timestamps = false;

    protected $fillable = [
        'task_id', 'ceo_id', 'proof_image_url', 'remark', 'is_sent_to_smm', 'created_at', 'submitted_at'
    ];

    protected $casts = [
        'task_id' => 'integer',
        'ceo_id' => 'integer',
        'is_sent_to_smm' => 'boolean'
    ];

    protected $appends = [
        'type'
    ];

    protected static function boot()
    {
        parent::boot();

        SocialTaskEntry::deleted(function($socialTaskEntry){

            if (! empty($socialTaskEntry->attributes['proof_image_url']))
            {
                $path       = config('uploads.social-proof') .'/'. $socialTaskEntry->ceo_id . '/' . $socialTaskEntry->attributes['proof_image_url'];
                $publicPath = public_path($path);

                if (File::exists($publicPath)){

                    File::delete($publicPath);

                }
            }

        });

    }

    public function getProofImageUrlAttribute($value)
    {
        $path       = config('uploads.social-proof') .'/'. $this->ceo_id . '/' . $value;

        $publicPath = public_path($path);
        $url        = url($path);

        if (! empty($value) && File::exists($publicPath)){
            return $url;
        }

        return '';

    }

    public function getDownloadProofImageUrlAttribute()
    {
        $path       = config('uploads.social-proof') .'/'. $this->ceo_id . '/' . $this->attributes['proof_image_url'];

        $publicPath = public_path($path);
        $url        = url('download/'.$path);

        if (File::exists($publicPath)){
            return $url;
        }

        return '';

    }

    public function getTypeAttribute()
    {
        $path       = config('uploads.social-proof') .'/'. $this->ceo_id . '/' . $this->attributes['proof_image_url'];

        $publicPath = public_path($path);

        if (File::exists($publicPath) && $mime = File::mimeType($publicPath)){
            return $mime;
        }

        return '';

    }

    public function task(){
        return $this->belongsTo('App\SocialTask', 'task_id', 'id');
    }
}
