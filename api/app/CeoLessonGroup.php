<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CeoLessonGroup extends Model
{
    protected $table = 'ceo_lesson_groups';

    protected $fillable = [
        'title'
    ];

    protected $casts = [
        'account_id' => 'integer'
    ];

    protected static function boot()
    {
        parent::boot();

        CeoLessonGroup::deleting(function($ceoLessonGroup){

            $ceoLessonGroup->lessons()->get()->each(function ($lesson){
                $lesson->delete();
            });

        });
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'account_id', 'id');
    }

    public function lessons()
    {
        return $this->hasMany('App\CeoLesson', 'group_id', 'id');
    }
}
