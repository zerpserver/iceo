<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'social_media_manager_email', 'is_admin', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'is_admin'
    ];

    protected $casts = [
        'count_apprentices' => 'integer',
        'count_deals' => 'integer',
        'profit_deals' => 'integer',
        'is_admin' => 'boolean',
        'active' => 'boolean'
    ];

    protected static function boot()
    {
        parent::boot();

        User::deleting(function($user){

            $user->apprentice_groups()->get()->each(function($apprenticeGroup){
                $apprenticeGroup->delete();
            });

            $user->lesson_groups()->get()->each(function ($lessonGroup){
                $lessonGroup->delete();
            });

            $user->social_task_entries()->get()->each(function ($socialTaskEntry){
                $socialTaskEntry->delete();
            });

        });
    }

    public function target()
    {
        return $this->hasOne('App\CeoTarget', 'account_id', 'id');
    }

    public function goals()
    {
        return $this->hasMany('App\CeoGoal', 'user_id', 'id');
    }

    public function notes()
    {
        return $this->hasMany('App\CeoNote', 'user_id', 'id')->orderBy('id', 'DESC');
    }

    public function apprentice_groups()
    {
        return $this->hasMany('App\ApprenticeGroup', 'account_id', 'id');
    }

    public function lesson_groups()
    {
        return $this->hasMany('App\CeoLessonGroup', 'account_id', 'id');
    }

    public function social_task_entries()
    {
        return $this->hasMany('App\SocialTaskEntry', 'ceo_id', 'id');
    }
}
