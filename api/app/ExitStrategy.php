<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExitStrategy extends Model
{
    protected $table = 'exit_strategies';

    protected $fillable = [
        'title'
    ];

    public $timestamps = false;
}
