<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprenticeGroup extends Model
{
    protected $table = 'apprentices_groups';

    protected $fillable = [
        'account_id',
        'title',
        'can_remove'
    ];

    protected $casts = [
        'account_id' => 'integer',
        'apprentices_count' => 'integer',
        'can_remove' => 'boolean'
    ];

    protected $hidden = [
        'count_as_lost'
    ];

    protected static function boot()
    {
        parent::boot();

        ApprenticeGroup::deleting(function($apprenticeGroup){

            $apprenticeGroup->apprentices()->get()->each(function ($apprentice){
                $apprentice->delete();
            });

        });
    }

    public function apprentices(){
        return $this->hasMany('App\Apprentice', 'group_id', 'id');
    }
}
