<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CeoCalendar extends Model
{
    protected $table = 'ceo_calendar';

    protected $fillable = [
        'title', 'description', 'due_date'
    ];

    protected $casts = [
        'account_id' => 'integer'
    ];

    protected $dates = ['due_date'];

    public function setDueDateAttribute($date){
        $this->attributes['due_date'] = Carbon::parse($date)->format('Y-m-d');
    }

    public function getDueDateAttribute($date){
        return $this->attributes['due_date'] = Carbon::parse($date)->format('Y-m-d');
    }

    public function user(){
        return $this->belongsTo('App\User', 'account_id', 'id');
    }
}
