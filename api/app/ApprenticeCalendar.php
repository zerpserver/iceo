<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;

class ApprenticeCalendar extends Model
{
    protected $table = 'apprentice_calendar';

    protected $fillable = [
        'apprentice_id',
        'image',
    ];

    protected $casts = [
        'apprentice_id' => 'integer'
    ];

    protected static function boot()
    {
        parent::boot();

        ApprenticeCalendar::deleted(function($apprenticeCalendar){

            if (! empty($apprenticeCalendar->attributes['image']))
            {
                $path       = config('uploads.calendar') .'/'. $apprenticeCalendar->apprentice_id . '/' . $apprenticeCalendar->attributes['image'];
                $publicPath = public_path($path);

                if (File::exists($publicPath)){

                    File::delete($publicPath);

                }
            }

        });

    }

    public function getImageAttribute($value)
    {
        $path       = config('uploads.calendar') .'/'. $this->apprentice_id . '/' . $value;

        $publicPath = public_path($path);
        $url        = url($path);

        if (! empty($value) && File::exists($publicPath)){
            return $url;
        }

        return '';
    }

    public function apprentice(){
        return $this->belongsTo('App\Apprentice', 'apprentice_id', 'id');
    }
}
