<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CeoNote extends Model
{
    protected $table = 'ceo_notes';

    protected $fillable = [
        'user_id', 'note'
    ];

    protected $casts = [
        'user_id' => 'integer'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
