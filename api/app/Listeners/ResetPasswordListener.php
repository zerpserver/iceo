<?php

namespace App\Listeners;

use App\Events\ResetPassword;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  ResetPassword  $event
     * @return void
     */
    public function handle(ResetPassword $event)
    {
        $this->mailer->send('emails.reset-password',$event->data, function($message) use ($event){

            $message->to($event->data['user']->email)->subject('Reset Password');

        });
    }
}
