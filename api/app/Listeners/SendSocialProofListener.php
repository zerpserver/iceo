<?php

namespace App\Listeners;

use App\Events\SendSocialProof;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use JWTAuth;

class SendSocialProofListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  SendSocialProof  $event
     * @return void
     */
    public function handle(SendSocialProof $event)
    {
        $user = JWTAuth::parseToken()->toUser();

        $event->data['user'] = $user;

        $this->mailer->send('emails.social-proofs',$event->data, function($message) use ($user){

            $message->to($user->social_media_manager_email, 'Social Media Manager')->subject('Social Proofs');

        });
    }
}
