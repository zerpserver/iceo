<?php

namespace App\Listeners;

use App\Events\SendUserWelcome;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserWelcomeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  SendUserWelcome  $event
     * @return void
     */
    public function handle(SendUserWelcome $event)
    {
        $this->mailer->send('emails.user-welcome',$event->data, function($message) use ($event){

            $message->to($event->data['email'], $event->data['name'])->subject('Welcome');

        });
    }
}
